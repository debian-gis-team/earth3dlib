#ifndef EARTH3DWIDGET_H
#define EARTH3DWIDGET_H

#include <QGLWidget>
#include <QWaitCondition>

#include "mousenavigator.h"
#include "graphicsObjectsContainer.h"
#include "network/downloadfinishedlistener.h"
#include "floatimage.h"

/**
 * This widget draws the 3D view of the earth. Important methods are
 * setNavigator to add some navigation functions to the earth (like MouseNavigator)
 * and addDrawObject to add own objects to the scene.
 */
class Earth3dWidget : public QGLWidget, public DownloadFinishedListener {
protected:
	float aspect;
	bool viewCulling;
	GraphicsObjectsContainer goc;

	/** The position of the camera is controlled by this Navigator. */
	Navigator *nav;

	QMutex rendering;

	ConnectNetworkService *cns;
	
	bool matrix_valid;

	double proj[16]; // This will hold our projection matrix
	double modl[16]; // This will hold our modelview matrix
	GLint viewport[4];

	/* get depth for mouse rotation */
	FloatImage floatImage;
	float clippingPlanes[2];
	bool grabDepthBuffer;

public:
	Earth3dWidget(QWidget *parent);
	~Earth3dWidget();
	void setSource(QString url);
	virtual void paintGL();
    virtual void resizeGL(int width, int height);
    virtual void addDrawObject(Draw *draw);
    virtual void keyPressEvent(QKeyEvent * e);
    virtual void mouseMoveEvent(QMouseEvent * e);
    virtual void mousePressEvent(QMouseEvent * e);
    virtual void mouseReleaseEvent(QMouseEvent * e);
    virtual void wheelEvent( QWheelEvent * e );
    virtual void contextMenuEvent(QContextMenuEvent * e);
    virtual void downloadFinished(DownloadFinishedEvent dfe);
    /**
     * Sets the class that handles the movement of the earth.
     */
    virtual void setNavigator(Navigator *nav);

    /**
     * Sets the address from where to retrieve the texture tree and the heightfield
     * tree for the visualization of the earth. Adds a new Draw object with the given
     * addresses to the widget.
     */
    virtual void setEarthSource(char *textureurl, char *heightfieldurl);

    /**
     * Contains Z Buffer information of the view.
     */
    virtual FloatImage *getDepthBuffer();
    virtual float *getClippingPlanes();
    virtual GLint *getViewport();
    virtual double *getProjectionMatrix();
    virtual double *getModelviewMatrix();
    virtual void setGrabDepthBuffer(bool grab);
    virtual bool getMatrixValid();

protected:
	void init();
	virtual void initializeGL();
	void render();
};

#endif
