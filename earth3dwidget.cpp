#include <earth3dwidget.h>
#include "draw/treeDraw.h"
#include "draw/treeDrawSphere.h"
#include "tree/textureTreeNodeCore.h"
#include "tree/heightfieldTreeNodeCore.h"
#include "network/connectNetworkService.h"

#include <QKeyEvent>

#ifdef WIN32
#include "wingl.h"
#endif

/**
 * The main Earth3d widget that displays the earth and additional objects that
 * are added by using addDrawObject.
 */


/* white ambient light at full intensity (rgba) */
GLfloat ELightAmbient[] = { 1.0f, 1.0f, 1.0f, 1.0f };

/* super bright, full intensity diffuse light. */
GLfloat ELightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };

/* position of light (x, y, z, (position of light)) */
GLfloat ELightPosition[] = { 1.f, 0.0f, 3.0f, 0.0f };
GLfloat Elight0_specular[] = { 1, 1, 1, 1 };
GLfloat Emat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat Emat_shininess[] = { 50.0 };

Earth3dWidget::Earth3dWidget(QWidget *parent) :
	QGLWidget(parent) {
    
	init();
}

Earth3dWidget::~Earth3dWidget() {
	if (floatImage.buffer)
		delete[](floatImage.buffer);
}

void Earth3dWidget::init() {
	clippingPlanes[0]=10;
	clippingPlanes[1]=10;

	viewCulling = true;
	setFocusPolicy(Qt::StrongFocus);
	setAutoBufferSwap(false);
	this->format().setDoubleBuffer(true);

	aspect = 1.;

	alwaysBind = false;

	matrix_valid = false;

	floatImage.buffer = NULL;
	grabDepthBuffer = true;


	cns = new ConnectNetworkService();
	
	// Add a listener to start a redraw after every successful download
	cns->addDownloadFinishedListener(this);
}

void Earth3dWidget::setEarthSource(char *textureurl, char *heightfieldurl) {
    // Create earth object
    MapTileTree *newMapTileTree = new MapTileTree();
    MapTileTreeNode *newRootNode = newMapTileTree->getRootNode();

    TextureTreeNodeCore *tCore = new TextureTreeNodeCore();
    tCore->setRequestID((QString("<connections><url address=\"")+QString(textureurl)+QString("\" /></connections>")).toLatin1().data());
    HeightfieldTreeNodeCore *hCore = new HeightfieldTreeNodeCore();
    hCore->setRequestID((QString("<connections><url address=\"")+QString(heightfieldurl)+QString("\" /></connections>")).toLatin1().data());

    newRootNode->setCore(0, tCore);
    newRootNode->setCore(1, hCore);
    
    // Add the earth
    addDrawObject(new TreeDrawSphere(newMapTileTree, cns, "sphere", false));
}

void Earth3dWidget::addDrawObject(Draw *draw) {
	goc.add(draw);
}

void Earth3dWidget::initializeGL() {
}

void Earth3dWidget::paintGL() {
#ifdef WIN32
	initGLARB();
#endif

	if (!rendering.tryLock())
		return;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//   printf("****** ASPECT: %f\n", aspect);

	float nearplane = nav->viewer.length()-1.3;
	if (nearplane<0.00001f)
		nearplane=0.00001f;
	float farplane = 20.f;
	if (nearplane<1.5)
		farplane = 10;

#ifdef WIN32_ZBUFFER_BUG
	nearplane = nav->viewer.length()-1.0001;
	if (nearplane<0.00001f) nearplane=0.00001f;
	if (nearplane<1.3) farplane = 2;
#endif
	//   if (nav->viewer.length()<1.1) farplane = 0.5;
	if (nav->viewer.length()<1.1)
		farplane = 1.5;
	gluPerspective(45.0f, aspect, nearplane, farplane);
	//   gluPerspective(45.0f,aspect,0.01f,10.0f);
	Point3D viewpoint = nav->direction + nav->viewer;
	gluLookAt(nav->viewer.x, nav->viewer.y, nav->viewer.z, viewpoint.x,
			viewpoint.y, viewpoint.z, nav->up.x, nav->up.y, nav->up.z);
	//  glFrustum( -1.0, 1.0, -1.0, 1.0, 10.0, 100.0 );

	render();

	swapBuffers();

	rendering.unlock();
}

void Earth3dWidget::render() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity(); // Reset The View

	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
	float colorfactor = nav->viewer.length()-1;
	colorfactor *= colorfactor;
	if (colorfactor<=1)
		colorfactor=1.f;

	glClearColor(0.3f/colorfactor, 0.6f/colorfactor, 0.9f/colorfactor, 1.0f);

	glEnable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
	glEnable(GL_NORMALIZE);

	glShadeModel(GL_SMOOTH);

	glLightfv(GL_LIGHT1, GL_AMBIENT, ELightAmbient); // add lighting. (ambient)
	glLightfv(GL_LIGHT1, GL_DIFFUSE, ELightDiffuse); // add lighting. (diffuse).
	glLightfv(GL_LIGHT1, GL_POSITION, ELightPosition); // set light position.
	glLightfv(GL_LIGHT1, GL_SPECULAR, Elight0_specular);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_LIGHT1); // turn light 1 on.

	/* read environment */
	glGetDoublev(GL_PROJECTION_MATRIX, proj);
	glGetDoublev(GL_MODELVIEW_MATRIX, modl);
	glGetIntegerv(GL_VIEWPORT, viewport);
	matrix_valid = true;

	/* draw phase 0 (background) to 1 (main layer, earth globe) */
	int listsize = goc.getListSize();
	for (int phase=0; phase<2; phase++) {
		for (unsigned int nr=0; nr<listsize; nr++) {
			Draw *td = goc.getListElement(nr);
			td->draw(&nav->viewer, viewCulling, this, nav->direction, phase);
		}
	}

	if (grabDepthBuffer) {
		/* read depth buffer */
		if (!floatImage.buffer) {
			floatImage.width = size().width();
			floatImage.height = size().height();
			floatImage.buffer = new float[floatImage.width * floatImage.height];
		} else {
			if (size().width()!=floatImage.width || size().height()!=floatImage.height) {
				delete[](floatImage.buffer);
	            floatImage.width = size().width();
	            floatImage.height = size().height();
	            floatImage.buffer = new float[floatImage.width * floatImage.height];
			}
		}
		glReadPixels(0, 0, floatImage.width, floatImage.height, GL_DEPTH_COMPONENT, GL_FLOAT, floatImage.buffer);
	}

	glGetFloatv(GL_DEPTH_RANGE, clippingPlanes);

	/* draw phase 2 (sky/sign/object layer) */
	for (unsigned int nr=0; nr<listsize; nr++) {
		Draw *td = goc.getListElement(nr);
		td->draw(&nav->viewer, viewCulling, this, nav->direction, 2);
	}

	/* draw phase 3 (head up layer, no perspective transformation) */
	/* change projection matrix */
	glClear(GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(-1, 1, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	listsize = goc.getListSize();
	for (unsigned int nr=0; nr<listsize; nr++) {
		Draw *td = goc.getListElement(nr);
		td->draw(&nav->viewer, viewCulling, this, nav->direction, 3);
	}
}

void Earth3dWidget::resizeGL(int width, int height) {
	QGLWidget::resizeGL(width, height);

	glViewport(0, 0, width, height);
	glEnable(GL_DEPTH_TEST);
	aspect=float(width)/float(height);
}

void Earth3dWidget::keyPressEvent(QKeyEvent * e) {
	bool repaint = nav->keyPressEvent(e);
	
	if (repaint) {
		updateGL();

		e->ignore();
	}
}

void Earth3dWidget::mouseMoveEvent(QMouseEvent * e) {
    nav->mouseMoveEvent(e);
}

void Earth3dWidget::mousePressEvent(QMouseEvent * e) {
    nav->mousePressEvent(e);
}

void Earth3dWidget::mouseReleaseEvent(QMouseEvent * e) {
    nav->mouseReleaseEvent(e);
}

void Earth3dWidget::wheelEvent( QWheelEvent * e ) {
    nav->wheelEvent(e);
}

void Earth3dWidget::contextMenuEvent(QContextMenuEvent * e) {
    nav->contextMenuEvent(e);
}

void Earth3dWidget::downloadFinished(DownloadFinishedEvent dfe) {
    updateGL();
}

void Earth3dWidget::setNavigator(Navigator *nav) {
    this->nav = nav;
}

FloatImage * Earth3dWidget::getDepthBuffer() {
    return &floatImage;
}

float *Earth3dWidget::getClippingPlanes() {
    return clippingPlanes;
}

GLint *Earth3dWidget::getViewport() {
    return viewport;
}

double *Earth3dWidget::getProjectionMatrix() {
    return proj;
}

double *Earth3dWidget::getModelviewMatrix() {
    return modl;
}

void Earth3dWidget::setGrabDepthBuffer(bool grab) {
    grabDepthBuffer = grab;
}

bool Earth3dWidget::getMatrixValid() {
    return matrix_valid;
}
