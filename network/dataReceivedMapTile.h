#ifndef DATARECEIVEDMAPTILE_H
#define DATARECEIVEDMAPTILE_H
#include "dataReceivedListener.h"

class DataReceivedMapTile : public DataReceivedListener {

 public:
  DataReceivedMapTile();
  virtual ~DataReceivedMapTile();

  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);
};

#endif
