#ifndef DOWNLOADFINISHEDLISTENER_H_
#define DOWNLOADFINISHEDLISTENER_H_
#include "downloadFinishedEvent.h"

class DownloadFinishedListener {
public:
    virtual void downloadFinished(DownloadFinishedEvent dfe) = 0;
};

#endif
