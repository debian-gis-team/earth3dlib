#include "downloadFinishedEvent.h"
//Added by qt3to4:
#include <QEvent>

DownloadFinishedEvent::DownloadFinishedEvent(URLDownload *urlDownload)
  : QEvent((QEvent::Type) 1003) {
  this->urlDownload = urlDownload;
}

DownloadFinishedEvent::~DownloadFinishedEvent() {
}

URLDownload *DownloadFinishedEvent::getURLDownload() {
  return(urlDownload);
}
