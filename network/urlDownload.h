#ifndef URLDOWNLOAD_H
#define URLDOWNLOAD_H
#include <qthread.h>
#include <q3urloperator.h>
#include "dataReceivedListener.h"
#include "stopableDownload.h"
#include "proxyUrlOperator.h"

class URLDownload : public ProxyUrlOperator, public StopableDownload {
  Q_OBJECT

 protected:
  char *content;
  int contentsize;
  DataReceivedListener *drl;
  void *userdata;
  char *thisurl;
  int retrynr;

  virtual void finished(Q3NetworkOperation *qno);

 public:
  URLDownload(const char *url, DataReceivedListener *drl, void *userdata, bool allowCache=true);
  virtual ~URLDownload();

  virtual void run();

 public slots:
  void finished_download(Q3NetworkOperation *qno);
  void data_received(const QByteArray &array, Q3NetworkOperation *qno);
};

#endif
