#include "urlDownload.h"
#include "urlTools.h"
#include <string.h>
#include <iostream>
#include <q3network.h>
#include <q3networkprotocol.h>
#include <qdatetime.h>
#include <stdio.h>
#include "statusObserver.h"
#include <qfile.h>
#include "downloadFinishedEvent.h"

#ifndef WIN32
#include <alloc.h>
#endif

using namespace std;

URLDownload::URLDownload(const char *url, DataReceivedListener *drl,
        void *userdata, bool allowCache) :
    ProxyUrlOperator(url, allowCache), StopableDownload() {
    connect(this, SIGNAL(finished(Q3NetworkOperation *)), this, SLOT(finished_download(Q3NetworkOperation *)));
    connect(this, SIGNAL(data(const QByteArray &, Q3NetworkOperation *)), this, SLOT(data_received(const QByteArray &, Q3NetworkOperation *)));

    content = (char *) malloc(1000);
    contentsize = 0;

#ifdef EARTH3DDEBUG
    cout << "URL: " << url << endl;
#endif

    thisurl = new char[strlen(url)+1];
    strcpy(thisurl, url);

    this->drl = drl;
    this->userdata = userdata;
    this->retrynr = 0;

    registerWithDownloadable(drl);
}

URLDownload::~URLDownload() {
    if (thisurl)
        delete[](thisurl);
}

void URLDownload::run() {
#ifdef EARTH3DDEBUG
    printf("URLDownload::run\n");
#endif

    // QT has problems with file://-URLs, download them via normal File IO:
    if (QString(thisurl).left(8)==QString("file:///")) {
#ifdef EARTH3DDEBUG
        printf("URLDownload::run file:\n");
#endif
        QString filename = QString(thisurl).mid(8);
        QFile datafile(filename);
        if (!datafile.exists()) {
            return;
        }
        datafile.open(QIODevice::ReadOnly);
        printf("File size: %i\n", datafile.size());
        data_received(datafile.readAll(), NULL);
        datafile.close();
       	finished(NULL);

        //    qApp->postEvent(form, new DownloadFinishedEvent(this));
    } else {
#ifdef EARTH3DDEBUG
        printf("URLDownload::run get\n");
#endif
        get();
    }
}

void URLDownload::finished(Q3NetworkOperation *qno) {
#ifdef EARTH3DDEBUG
    printf("URLDownload::finished\n");
#endif

    if (contentsize<=0) {
        // the download was not successful
        drl->dataReceived(NULL, 0, thisurl, 0, NULL, userdata, NULL, this);
        return;
    }

    /* parse contents of file into header and parts */
    char *cur = (char *) content, *cr;
    int countParts;
    struct part_t *parts;

    cr = strchr(cur, '\n');
    countParts = atoi(cur);
#ifdef DEBUG
    cout << "countParts: " << countParts << endl;
#endif

    if (countParts==0) { // try to parse it as single XML document
        parts = new part_t[1];

        parts[0].content = (char *) content;
        parts[0].size = contentsize;
        countParts = 1;
    } else {
        parts = new part_t[countParts];

        if (cr) {
            cur = cr+1;
            for (int partnr = 0; partnr<countParts; partnr++) {
                parts[partnr].size = atoi(cur);
#ifdef EARTH3DDEBUG
                cout << "part " << partnr << " size " << atoi(cur) << endl;
#endif

                cr = strchr(cur, '\n');
                if (cr) {
                    cur = cr+1;
                    parts[partnr].content = new char[parts[partnr].size+1];

                    memcpy(parts[partnr].content, cur, parts[partnr].size);
                    parts[partnr].content[parts[partnr].size]=0;
                    cur += parts[partnr].size;
                } else {
                    parts[partnr].content = NULL;
                }
            }
        }

        free(content);
        content = NULL;
    }

#ifdef DEBUG
    cout << "part0 size" << parts[0].size << endl;
#endif

    /* rewrite relative URLs */
    UrlTools::rewriteRelativeURLs(&(parts[0].content), &(parts[0].size), thisurl);

    /* call DataReceivedListener */
#ifdef DEBUG
    cout << "part0 size" << parts[0].size << endl;
#endif

    drl->dataReceived(parts[0].content, parts[0].size, thisurl, countParts,
            parts, userdata, NULL, this);
    std::cout << QTime::currentTime().toString("hh:mm:ss:zzz").latin1() << ": DataReceived finished: " << thisurl
            << std::endl;

    /* clean up */
    for (int partnr=0; partnr<countParts; partnr++) {
        if (parts[partnr].content)
            delete[](parts[partnr].content);
    }
    delete[](parts);

    // delete this object
    if (thisurl) {
        delete[](thisurl);
        thisurl = NULL;
    }
}

void URLDownload::finished_download(Q3NetworkOperation *qno) {
    if (!downloadStopped) {
        // try again, 5 times
        if (qno!=NULL && qno->state()==Q3NetworkProtocol::StFailed) { //qno->errorCode()) {
            cout << "retry: " << thisurl << endl;
            retrynr++;
            if (retrynr<5) {
                get();
                return;
            } else {
                drl->dataReceived(NULL, 0, thisurl, 0, NULL, userdata, NULL, this);
                deregisterWithDownloadable(drl);
            }
        } else {
            statusobserver.changeMemoryOffset(StatusObserver::MEM_URLDOWNLOAD,
                    contentsize);

            finished(qno);
            deregisterWithDownloadable(drl);
        }
    }

#ifndef WIN32
    /* try to delete self */
    // removed since QT4 delete(this);
#endif
}

void URLDownload::data_received(const QByteArray &array,
        Q3NetworkOperation *qno) {
    //   char formatline[256];
    //   sprintf(formatline, "%%%is\\n", array.size());
    //   printf(formatline, array.data());

#ifdef EARTH3DDEBUG
    printf("data_received %i data: %s\n", array.size(), QString(QByteArray(array.data(), array.size())).latin1());
#endif
    contentsize += array.size();
    content = (char *) realloc(content, contentsize);
    memcpy(content + contentsize - array.size(), array.data(), array.size());
}

