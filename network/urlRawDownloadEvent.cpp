#include "urlRawDownloadEvent.h"
//Added by qt3to4:
#include <QEvent>

URLRawDownloadEvent::URLRawDownloadEvent(URLRawDownload *uRawDownload)
  : QEvent((QEvent::Type) 1002) {
  this->uRawDownload = uRawDownload;
}

URLRawDownloadEvent::~URLRawDownloadEvent() {
}

URLRawDownload *URLRawDownloadEvent::getURLRawDownload() {
  return(uRawDownload);
}
