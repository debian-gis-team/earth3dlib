#ifndef QSERVICELIB_H
#define QSERVICELIB_H
#include <q3socketdevice.h>
#include <q3dns.h>

struct part_t {
  int size;
  char *content;
};

class ServiceLib : public QObject {
  Q_OBJECT

  char identity[256];
  Q3SocketDevice socketdevice;
  Q3Dns dns;
  int port;

  void getline(char *line, int size, int timeout);
  int getnumber(int timeout);

  void disconnect();

 public:
  ServiceLib();
  virtual ~ServiceLib();

  int readParts(char *senderID, int senderIDSize, struct part_t *parts, int maxParts, int timeout=-1);
  static struct part_t *copyParts(struct part_t *parts, int countParts);
  static void freeParts(struct part_t *parts, int countParts);
};

#endif
