#include "dataReceivedMapTile.h"
#include <iostream>
#include <qdom.h>
//Added by qt3to4:
#include <QTextStream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#include "connectNetworkService.h"
#include "mapTileTreeNodeCore.h"
#include "textureTreeNodeCore.h"
#include "heightfieldTreeNodeCore.h"
#include "treeDrawSphere.h"
#include "mutex.h"

#ifndef WIN32
#include <unistd.h>
#endif

// DATARECEIVEDMAPTILE
// <response msgid="2">
// <maptile attachment="1" sizex="256" sizey="256" pos="0"><submaptile relpos="leftUp"><connections><agent remoteid="538035540" agent="#&lt;Agent:0x4025f3c8&gt;"><agent agent="#&lt;ExternalProcess:0x4027054c&gt;"><maptile pos="8"/></agent></agent></connections></submaptile><submaptile relpos="rightUp"><connections><agent remoteid="538035540" agent="#&lt;Agent:0x4025f3c8&gt;"><agent agent="#&lt;ExternalProcess:0x4027054c&gt;"><maptile pos="1"/></agent></agent></connections></submaptile><submaptile relpos="leftDown"><connections><agent remoteid="538035540" agent="#&lt;Agent:0x4025f3c8&gt;"><agent agent="#&lt;ExternalProcess:0x4027054c&gt;"><maptile pos="50"/></agent></agent></connections></submaptile><submaptile relpos="rightDown"><connections><agent remoteid="538035540" agent="#&lt;Agent:0x4025f3c8&gt;"><agent agent="#&lt;ExternalProcess:0x4027054c&gt;"><maptile pos="43"/></agent></agent></connections></submaptile></maptile>
// </response>

DataReceivedMapTile::DataReceivedMapTile() {
}

DataReceivedMapTile::~DataReceivedMapTile() {
}

void DataReceivedMapTile::dataReceived(const char *xmlresponse, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download) {
  if (xmlresponse==NULL) { // could not be loaded
    /* write information to node */
    MapTileTreeNodeCore *core = (TextureTreeNodeCore *) userdata;
    core->setGenerated(false);
    core->setDownloaded(true);
    core->setDownloadFailed();
    return;
  }

  // parse the document
  QDomDocument doc;
  if (doc.setContent(QString::fromLatin1(xmlresponse, size))) {
    /* search for response tag */
    QDomNode nresponse = doc.documentElement();
    while( !nresponse.isNull() ) {
      QDomElement e = nresponse.toElement(); // try to convert the node to an element.
      if( !e.isNull() && e.tagName() == QString("response")) {
       
	QDomNode nmaptile = nresponse.firstChild();
	while( !nmaptile.isNull() ) {
	  e = nmaptile.toElement(); // try to convert the node to an element.
	  if( !e.isNull() && e.tagName() == QString("maptile")) {

	    /* write information to node */
	    MapTileTreeNodeCore *core = (MapTileTreeNodeCore *) userdata;

	    /* first read the binary data */
	    int attachment = e.attribute(QString("attachment")).toInt(); // get the attachment number
	    int sizex =      e.attribute(QString("sizex")).toInt(); // get the size in x
	    int sizey =      e.attribute(QString("sizey")).toInt(); // get the size in y
	    QString imageType = e.attribute(QString("type"));

// 	    // DEBUG: write the attachment to disk
// 	    int handle = open("attachment.dat", O_CREAT | O_WRONLY | O_TRUNC, S_IREAD|S_IWRITE);
// 	    write(handle, parts[attachment].content, parts[attachment].size);
// 	    close(handle);

	    /* go through submaptiles */
	    QDomNode nsubmaptile = nmaptile.firstChild();
	    while( !nsubmaptile.isNull() ) {
	      e = nsubmaptile.toElement(); // try to convert the node to an element.
	      if( !e.isNull() && e.tagName() == QString("submaptile")) {
		QString relpos = e.attribute(QString("relpos"));

		int submapchildnr = 0;

		if (relpos == QString("leftUp"))    submapchildnr = 2;
		if (relpos == QString("rightUp"))   submapchildnr = 0;
		if (relpos == QString("leftDown"))  submapchildnr = 3;
		if (relpos == QString("rightDown")) submapchildnr = 1;

		/* search for connections object */
		QDomNode n4 = nsubmaptile.firstChild();
		while( !n4.isNull() ) {
		  if (n4.isElement() && n4.toElement().tagName() == QString("connections")) {
		    QString output;
		    QTextStream ts(&output, QIODevice::WriteOnly);
		    ts << n4;
// 		    cout << "dataReceivedMapTile " << output.latin1() << " END " << endl;

		    /* set the connection in the core */
		    assert(core->magic==45678);
		    core->setChildRequestID(submapchildnr, output.latin1());
		  }
		  
		  n4 = n4.nextSibling();
		}
	      }

	      nsubmaptile = nsubmaptile.nextSibling();
	    }

	    core->setImage(sizex, sizey, parts[attachment].content, parts[attachment].size, imageType.latin1());
	    core->setGenerated(false);
	    core->setDownloaded(true);
	  }
	  nmaptile = nmaptile.nextSibling();
	}
      }
      nresponse = nresponse.nextSibling();
    }
  }
}

