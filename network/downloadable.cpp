#include "downloadable.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

Downloadable::Downloadable() {
  runningDownloads = 0;
  runningDownloadsArray = NULL;
}

Downloadable::~Downloadable() {
  if (runningDownloadsArray) delete[](runningDownloadsArray);
}

void Downloadable::addDownloader(StopableDownload *drl) {
  if (drl) {
    QMutexLocker qml(&downloadableMutex);
    runningDownloads++;
    runningDownloadsArray = (StopableDownload **) realloc(runningDownloadsArray, runningDownloads*sizeof(StopableDownload *));
    runningDownloadsArray[runningDownloads-1]=drl;
  }
}

void Downloadable::removeDownloader(StopableDownload *drl) {
  if (drl) {
    QMutexLocker qml(&downloadableMutex);
    int i;
    for(i=0; i<runningDownloads; i++) {
      if (runningDownloadsArray[i]==drl) {
	memmove(runningDownloadsArray+i, runningDownloadsArray+i+1, runningDownloads-i-1);
	runningDownloads--;
	i--;
      }
    }

    runningDownloadsArray = (StopableDownload **) realloc(runningDownloadsArray, runningDownloads*sizeof(StopableDownload *));
  }
}

void Downloadable::stopDownloads() {
  /* create a copy of the array to prevent deadlocks */
  downloadableMutex.lock();
  StopableDownload **tmpRunningDownloadsArray = (StopableDownload **) malloc(runningDownloads*sizeof(StopableDownload *));
  memcpy(tmpRunningDownloadsArray, runningDownloadsArray, runningDownloads*sizeof(StopableDownload *));
  int tmpRunningDownloads = runningDownloads;

  /* after stopping the downloads, the references are removed,
   * the connection between Downloadable and StopableDownload is
   * stopped.
   */
  runningDownloads = 0;
  free(runningDownloadsArray);
  runningDownloadsArray = NULL;

  /* stop all downloads that download data for this Downloadable */
  for(int i=0; i<tmpRunningDownloads; i++) {
    tmpRunningDownloadsArray[i]->stopDownload(this);
  }

  free(tmpRunningDownloadsArray);
  downloadableMutex.unlock();
}
