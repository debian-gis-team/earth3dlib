#include "fileCache.h"
#include "md5.h"
#include <QDir>
#include <QFile>
#include <QDomDocument>
#include <QByteArray>
#include <QTextStream>
#include <QMutexLocker>
#include <iostream>
#include "globalsettings.h"

using namespace std;

QMutex filesMutex(QMutex::Recursive);

FileCache::FileCache() {
}

/**
 * Sets the directory where the cache should be.
 */
void FileCache::setCacheLocation(QString path) {
  cacheLocation = path;
  QDir().mkpath(path);

  loadCache();
}

/**
 * Loads the cache from the location set bei setCacheLocation.
 */
void FileCache::loadCache() {
  QMutexLocker qml(&filesMutex);

  files.clear();
  filesizes.clear();

  QString filename = cacheLocation + QDir::separator() + QString("cache.index");
  QString alternativefilename = cacheLocation + QDir::separator() + QString("cache.index2");

  // try to load from index file
  loadCacheIndex(filename);

  // try to load from alternate index file (when the writing of the index file was interrupted)
  loadCacheIndex(alternativefilename);

  // check entries and remove non existing
  checkCacheIndex();

  // start thread that saves the list every two seconds if something has changed
  if (!isRunning()) {
    start();
  }
}

/**
 * Loads the cache from the given XML file and adds it to the cached files list.
 * So one can load several lists and they will be merged.
 */
void FileCache::loadCacheIndex(QString filename) {
  // try to load from filename

#ifdef DEBUG
  cout << "Cache path: " << filename.latin1() << endl;
#endif
  cacheIndexFilename = filename;

  // parse the document
  QDomDocument doc;
  QFile *file = new QFile(cacheIndexFilename);
  if (doc.setContent(file)) {
    /* search for earth3dcache tag */
    QDomNode nform = doc.documentElement();
    while( !nform.isNull() ) {
      QDomElement e = nform.toElement(); // try to convert the node to an element.
      if( !e.isNull() && e.tagName() == QString("earth3dcache")) {
	/* iterate through attribute tags */
	QDomNode ninput = nform.firstChild();
	while( !ninput.isNull() ) {
	  e = ninput.toElement(); // try to convert the node to an element.
	  if( !e.isNull() && e.tagName() == QString("cacheentry")) {
#ifdef DEBUG
	    cout << "filename: " << e.attribute("filename", "").latin1() << endl;
#endif
	    
	    /* insert into cache list  */
	    QString newfilename = e.attribute("filename", "");
	    if (newfilename != "" && !files.contains(newfilename)) {
	      files.append(newfilename);
	    }
	  }
	  
	  ninput = ninput.nextSibling();
	}
      }
      nform = nform.nextSibling();      
    }
  }
  
  file->close();
  delete(file);
}

/**
 * Iterates through all files and checks if they exist. Removes file entries
 * from the cache that are invalid.
 */
void FileCache::checkCacheIndex() {
  for(int i=0; i<files.size(); i++) {
    if (!QFile(cacheLocation + QDir::separator() + files[i]).exists()) {
      files.remove(i);
      i--;
    }
  }
}

/**
 * Saves the cache to an XML file. It is first saved into cache.index2,
 * then cache.index is removed and cache.index2 is renamed. On startup both
 * files are loaded so there cannot be a moment where no file is valid. The
 * program can be stopped at any time.
 */
void FileCache::saveCache() {
  // build XML document
  QDomDocument doc;

  QDomNode node = doc.createElement("earth3dcache");
  doc.appendChild(node);

  QVectorIterator<QString> it(files);
  while(it.hasNext()) {
    QDomElement attrnode = doc.createElement("cacheentry");
    attrnode.setAttribute("filename", it.next());

    node.appendChild(attrnode);
  }

  QDir().mkpath(cacheLocation);
  QFile file2(QDir(cacheLocation).filePath("cache.index2"));
  file2.open(QIODevice::WriteOnly);
  QTextStream textstream(&file2);
  textstream << doc.toString().latin1();
  file2.close();
  
  // remove cache.index and rename cache.index2 to cache.index
  QFile file(QDir(cacheLocation).filePath("cache.index"));
  file.remove();
  QDir().rename(QDir(cacheLocation).filePath("cache.index2"), QDir(cacheLocation).filePath("cache.index"));
}

/**
 * Returns true if the cache contains a file for the given URL.
 */
bool FileCache::contains(QString url) {
  QString filename = createFilename(url);
  return(files.contains(filename));
}

/**
 * Builds a filename for the given url. It uses an md5 sum where the
 * first characters are used as directory names to distribute the files
 * over several directories.
 */
QString FileCache::createFilename(QString url) {
  md5_context ctx;
  uint8 digest[16];

  // build md5 sum

  md5_starts(&ctx);
  md5_update(&ctx, (const unsigned char *) url.latin1(), url.size());
  md5_finish(&ctx, digest);

  // build string. The first three hex numbers are treated as directories to support filesystems that have limits
  // on how many files are allowed in one directory.

  QString result;
  for(int i=0; i<16; i++) {
    result.append(hexString(digest[i]));
    if (i<3) {
      result.append(QDir::separator());
    }
  }

  return(result);
}

/**
 * Creates a hex string from the given data array. It is used to create
 * a hex string from the calculated md5 sum.
 */
QString FileCache::hexString(uint8 value) {
  QString result = QString::number(value, 16);
  if (result.size()<2) {
    result = "0" + result;
  }

  return(result);
}

/**
 * Delivers the cached file for the given URL.
 */
QByteArray FileCache::read(QString url) {
  filesMutex.lock();

  QString filename = createFilename(url);
#ifdef DEBUG
  cout << "Cache hit: " << filename.latin1() << endl;
#endif
  int index = files.indexOf(filename);
  if (index>=0) {
    // move to top of the list
    files.remove(index);
    files.prepend(filename);

    filesMutex.unlock();

    // read file from disk

    QFile file(QDir(cacheLocation).filePath(filename));
    file.open(QIODevice::ReadOnly);
    QByteArray ba = file.readAll();
    file.close();

    return(ba);
  }

  filesMutex.unlock();

  QByteArray ba;
  return(ba);
}

/**
 * Adds the given data to the cache under the given url. It also checks the cache
 * size and removes files if necessary.
 */
void FileCache::addFile(QString url, QByteArray &ba) {
  QString filename = createFilename(url);

#ifdef DEBUG
  cout << "Cache add: " << filename.latin1() << endl;
#endif
  QString filepath = QDir(cacheLocation).filePath(filename);
  QString directory = QDir(filepath + QDir::separator() + "..").absolutePath();
#ifdef DEBUG
  cout << "Cache dir: " << directory.latin1() << endl;
#endif
  QDir().mkpath(directory);

  QFile file(filepath);
  file.open(QIODevice::WriteOnly);
  file.write(ba);
  file.close();

  QMutexLocker qml(&filesMutex);

  // insert the new file at the top of the list

  files.prepend(filename);
  dirty = true;

  checkCacheSize();

  saveCache();
}

/**
 * Checks if files from the cache must be deleted to fit the requested cache size.
 */
void FileCache::checkCacheSize() {
  QMutexLocker qml(&filesMutex);

  // sum up all file sizes
  qint64 size;
  size = getUsedSizeBytes();

  // check overall size

  qint64 cachesize = getAttribute("cachesize", "50").toInt() * 1024 * 1024;
  if (cachesize<1) cachesize = 1;

  // remove entries from the end until the size is fit

  while(size>cachesize && !files.isEmpty()) {
    QString lastfile = files.last();
    files.pop_back();

#ifdef DEBUG
    cout << "cache removing " << lastfile.latin1() << " current size: " << size << endl;
#endif
    QFile file(QDir(cacheLocation).filePath(lastfile));
    file.remove();
    dirty = true;
    size -= filesizes[lastfile];
    filesizes.remove(lastfile);
  }
}

/**
 * Returns the size of the given file either from the file system or
 * from the cache.
 */
int FileCache::getFileSize(QString filename) {
  QMutexLocker qml(&filesMutex);

  if (!filesizes.contains(filename)) {
    filesizes[filename] = QFile(QDir(cacheLocation).filePath(filename)).size();
  }

  return(filesizes[filename]);
}

/**
 * Returns the sum of the size of all files in the cache.
 */
qint64 FileCache::getUsedSizeBytes() {
  qint64 size = 0;

  QVectorIterator<QString> it(files);
  while(it.hasNext()) {
    QString filename = it.next();

    size += getFileSize(filename);
  }

  return(size);
}

float FileCache::getUsedSizeMB() {
  qint64 size = getUsedSizeBytes();

  // size in MB

  return(size/(1024.*1024.));
}

/**
 * Runs in a background thread. First loads the file sizes of all files and then
 * saves the cache every 2 seconds.
 */
void FileCache::run() {
	// load file sizes, this takes some time so do it in a thread
  getUsedSizeMB();
  
  while(true) {
    sleep(2);

    if (dirty) {
      QMutexLocker qml(&filesMutex);
      dirty = false;
      saveCache();
    }
  }
}
