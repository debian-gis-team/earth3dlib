#ifndef URLRAWDOWNLOAD_H
#define URLRAWDOWNLOAD_H

#include "urlDownload.h"

class URLRawDownload : public URLDownload {
  Q_OBJECT

 protected:
  virtual void finished(Q3NetworkOperation *qno);

 public:
  URLRawDownload(const char *url, DataReceivedListener *drl, void *userdata, bool allowCache=true);
  virtual ~URLRawDownload();
};

#endif
