#include "qServiceLib.h"
#include <QTextStream>
#include <Q3CString>
#include <string.h>
#include "connectNetworkService.h"
#include <vector>
#include <string>
#include <iostream>
#include <qdom.h>
#include "urlDownload.h"

#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif

using namespace std;

#define min(a,b) (a)>(b)?(b):(a)

ConnectNetworkService::ConnectNetworkService() :
    ConnectNetworkServiceRequestQueue(5) {
    currentMessageID = 0;
}

ConnectNetworkService::~ConnectNetworkService() {
}

void ConnectNetworkService::addDownloadFinishedListener(
        DownloadFinishedListener *dfl) {
    dflList.push_back(dfl);
}

/** Sends a request into the network */
void ConnectNetworkService::getOne(const char *xmlconnections,
        DataReceivedListener *drl, void *userdata, int timeout) {
    if (xmlconnections==NULL)
        return;
#ifdef EARTH3DDEBUG
    cout << "getOne " << xmlconnections << endl;
#endif

    /* parse the connections and choose one */
    char *connection = chooseConnection(xmlconnections);
    std::cout << "get connection2: " << std::endl;

    /* enqueue request until the request queue goes under a certain limit */
    push(new ConnectNetworkServiceRequestQueueObject(connection, drl, userdata, timeout, true));

    std::cout << "get connection end" << std::endl;
    delete[](connection);
}

void ConnectNetworkService::getURL(const char *xmlconnection,
        long currentMessageID, bool allowCache) {
#ifdef EARTH3DDEBUG
    cout << "Download URL: " << xmlconnection << endl;
#endif
    TimedDataReceivedListener *listener = getMapHandler(currentMessageID);

    listener->urldownload = new URLDownload(xmlconnection, this, (void *) currentMessageID, allowCache);
    listener->urldownload->run();
}

char *ConnectNetworkService::chooseConnection(const char *xmlconnections) {
    /* create DOM structure */
    QDomDocument doc;
    if (doc.setContent(QString(xmlconnections))) {
        QDomNode cur = doc.documentElement();
        if (cur.isElement() && cur.toElement().tagName() == QString("connections")) {
            QDomNode start = cur;

            /* search for agent or url connection */
            cur = start.firstChild();
            while (!cur.isNull()) {
                if (cur.isElement() && cur.toElement().tagName() == QString("url")) {

                    /* copy node into string */
                    QString output;
                    QTextStream ts(&output, QIODevice::WriteOnly);
                    ts << cur;

                    char *result = new char[output.length()+1];
                    strcpy(result, output.latin1());
                    return (result);
                }

                cur = cur.nextSibling();
            }
        }
    }

    return (NULL);
}

bool ConnectNetworkService::requestData(
        ConnectNetworkServiceRequestQueueObject *request) {
    int currentMessageID = request->getMessageID();

    cout << "requestData" << endl;
    // check the tag if it is an URL
    bool isURL = false;
    QString result;
    QString url;
    QDomDocument doc;
    if (doc.setContent(QString(request->getxmlconnection()))) {
        /* search for url tag */
        QDomNode n = doc.documentElement();
        while ( !n.isNull() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            if ( !e.isNull() && e.tagName() == QString("url")) {
                isURL = true;
                url = e.attribute("address");
                if (e.hasAttribute("allowCache")) {
                    request->setAllowCache(e.attribute("allowCache") == "true");
                }
            }
            n = n.nextSibling();
        }
    }

    if (isURL) {
        getURL(url.latin1(), currentMessageID, request->getAllowCache());
        return (false); // needs to stay in queue
    }

    cout << "ERROR: requestData: " << request->getxmlconnection() << endl;

    return (true);
}

void ConnectNetworkService::dataReceived(const char *response, int size,
        const char *sender, int countParts, struct part_t *parts,
        void *userdata, ConnectNetworkService *cns, StopableDownload *download) {
    if (response != NULL && size > 0) {
        fireDownloadFinished(download);
    }
    
    ConnectNetworkServiceRequestQueue::dataReceived(response, size, sender,
            countParts, parts, userdata, cns, download);
}

void ConnectNetworkService::fireDownloadFinished(StopableDownload *download) {
    cout << "FIREDOWNLOADFINISHED" << endl;
    
    for(unsigned int i=0; i<dflList.size(); i++) {
        DownloadFinishedListener *listener = dflList[i];
        
        listener->downloadFinished(DownloadFinishedEvent((URLDownload *) download));
    }
}
