#ifndef CONNECTNETWORKSERVICE_H
#define CONNECTNETWORKSERVICE_H
#include <vector>
#include <string>
#include <map>
#include "dataReceivedListener.h"
#include "downloadfinishedlistener.h"
#include <qthread.h>
#include "urlDownload.h"
#include "connectNetworkServiceRequestQueue.h"
#include <qdatetime.h>
#include <q3valuevector.h>

class QueuedMessage {
 public:
  QString receiver;
  QString message;

  QueuedMessage() {
  };

  QueuedMessage(QString receiver, QString message) {
    this->receiver = receiver;
    this->message = message;
  };

  QueuedMessage(const QueuedMessage &m) {
    this->receiver = m.receiver;
    this->message = m.message;
  };
};

/** This class retrieves data by using protocols like http and (optionally) other system.
 */
class ConnectNetworkService : public ConnectNetworkServiceRequestQueue {
  /** Vector to save the methods for addDownloadFinishedListener */
  std::vector<DownloadFinishedListener *> dflList;

  QTime requesttime;

  /** Chooses the best of all available connections and returns it. */
  char *chooseConnection(const char *xmlconnections);

  /** Requests a data object by using an URL */
  void getURL(const char *xmlconnection, long currentMessageID, bool allowCache);

  /** Receive URL data */
/*   virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata); */

 protected:
  /** Starts the request of request.
   *  @returns true if the request has been completed immediately (agent requests) or false, if it is delayed 
   */
  virtual bool requestData(ConnectNetworkServiceRequestQueueObject *request);

  /** Informs the listeners that a download was finished.
   */
  virtual void fireDownloadFinished(StopableDownload *download);

 public:
  ConnectNetworkService();
  virtual ~ConnectNetworkService();

  /** Request some data 
   * @param timeout in seconds
   */
  void getOne(const char *xmlconnections, DataReceivedListener *drl, void *userdata=NULL, int timeout=10);
  
  /** Adds a method that is called each time data was received. */
  void addDownloadFinishedListener(DownloadFinishedListener *dfl);
  
  /** Delivers finished downloads to the DownloadFinishedListeners */
  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);
};

#endif
