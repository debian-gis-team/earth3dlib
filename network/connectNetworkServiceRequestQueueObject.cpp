#include "connectNetworkServiceRequestQueueObject.h"
#include <string.h>
#include <stdlib.h>

ConnectNetworkServiceRequestQueueObject::ConnectNetworkServiceRequestQueueObject(const char *xmlconnection, DataReceivedListener *drl, void *userdata, int timeout, bool allowCache) {
  if (xmlconnection) {
    this->xmlconnection = new char[strlen(xmlconnection)+1];
    strcpy(this->xmlconnection, xmlconnection);
  }
  else {
    this->xmlconnection = NULL;
  }
  this->drl = drl;
  this->userdata = userdata;
  this->timeout = timeout;
  this->messageid = 0;
  this->allowCache = allowCache;
}

ConnectNetworkServiceRequestQueueObject::~ConnectNetworkServiceRequestQueueObject() {
  if (xmlconnection) delete[](xmlconnection);
}

int ConnectNetworkServiceRequestQueueObject::getTimeout() {
  return(timeout);
}

char *ConnectNetworkServiceRequestQueueObject::getxmlconnection() {
  return(xmlconnection);
}

DataReceivedListener *ConnectNetworkServiceRequestQueueObject::getDataReceivedListener() {
  return(drl);
}

void *ConnectNetworkServiceRequestQueueObject::getUserData() {
  return(userdata);
}

int ConnectNetworkServiceRequestQueueObject::getMessageID() {
  return(messageid);
} 

void ConnectNetworkServiceRequestQueueObject::setMessageID(int messageid) {
  this->messageid = messageid;
}

bool ConnectNetworkServiceRequestQueueObject::getAllowCache() {
  return(allowCache);
}

void ConnectNetworkServiceRequestQueueObject::setAllowCache(bool allowCache) {
  this->allowCache = allowCache;
}
