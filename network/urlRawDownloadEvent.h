#ifndef URLRAWDOWNLOADEVENT_H
#define URLRAWDOWNLOADEVENT_H
#include <qevent.h>
#include "urlRawDownload.h"

class URLRawDownloadEvent : public QEvent {
  URLRawDownload *uRawDownload;

 public:
  URLRawDownloadEvent(URLRawDownload *uRawDownload);
  ~URLRawDownloadEvent();

  URLRawDownload *getURLRawDownload();
};

#endif
