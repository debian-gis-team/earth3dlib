#ifndef DATARECEIVEDLISTENER_H
#define DATARECEIVEDLISTENER_H
#include "qServiceLib.h"
#include "downloadable.h"
#include "stopableDownload.h"

class ConnectNetworkService;

class DataReceivedListener : public Downloadable {
 public:
  DataReceivedListener();
  ~DataReceivedListener();

  /** Received data is delivered to this function on completition.
   * @param cns the ConnectNetworkService that received this datagram or NULL if it was received from an URL
   */
  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download) = 0;
};

#endif
