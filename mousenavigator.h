#ifndef MOUSENAVIGATOR_H_
#define MOUSENAVIGATOR_H_

#include "navigator.h"

/**
 * This navigator allows rotating and zooming the earth with the mouse.
 */
class MouseNavigator : public Navigator {
    bool old_valid;
    int mx, my;
    double old_sdx, old_sdy, old_sdz;

#ifdef WIN32
    bool mousepressed;
#endif
    
public:
    MouseNavigator(Earth3dWidget *earth);
    virtual ~MouseNavigator();

    virtual void mouseMoveEvent(QMouseEvent * e);
    virtual void mousePressEvent(QMouseEvent * e);
    virtual void mouseReleaseEvent(QMouseEvent * e);
    virtual void wheelEvent( QWheelEvent * e );
};
#endif /*MOUSENAVIGATOR_H_*/
