#include "globalsettings.h"

bool alwaysBind;
float MAXTILESIZE=1.4;
float CENTERWEIGHT=2;
bool flyMode = false;
bool needRedraw = false;
bool frameByFrame = false;

int maptiledrawtype;
float heightfieldmultiplier = 20;
int concurrentdownloads;

#include <QMap>
#include <QString>

QMap<QString, QString> configAttributesMap;

FileCache fileCache;

/* to prevent calling getAttributes once per frame, we cache two attributes here */
bool cacheUseMultiTexturing;
bool cacheUseTextureCompression;

#include <qdom.h>
#include <qfile.h>
#include <qdir.h>
#include <iostream>
#include <qtextstream.h>

using namespace std;

QString xmlconfigfilepath;

void readXMLConfig(QString path) {
  cout << "PATH: " << path.latin1() << endl;
  xmlconfigfilepath = QString(path.latin1());

  // parse the document
  QDomDocument doc;
  QFile *file = new QFile(QDir(path).filePath("earth3d.xml"));
  if (doc.setContent(file)) {
    /* search for earth3dconfig tag */
    QDomNode nform = doc.documentElement();
    while( !nform.isNull() ) {
      QDomElement e = nform.toElement(); // try to convert the node to an element.
      if( !e.isNull() && e.tagName() == QString("earth3dconfig")) {
	/* iterate through attribute tags */
	QDomNode ninput = nform.firstChild();
	while( !ninput.isNull() ) {
	  e = ninput.toElement(); // try to convert the node to an element.
	  if( !e.isNull() && e.tagName() == QString("attribute")) {
	    cout << "name: " << e.attribute("name", "").latin1() << " value: " << e.attribute("value", "").latin1() << endl;

	    /* insert into attribute map  */
	    configAttributesMap[e.attribute("name", "")] = e.attribute("value", "");
	  }

	  ninput = ninput.nextSibling();
	}
      }
      nform = nform.nextSibling();
    }
  }

  file->close();
  delete(file);
}

bool isAttributeExisting(QString attr) {
  return(configAttributesMap.contains(attr));
}

QString getAttribute(QString attr) {
  return(configAttributesMap[attr]);
}

QString getAttribute(QString attr, QString defaultvalue) {
  if (configAttributesMap.contains(attr)) {
    return(configAttributesMap[attr]);
  }

  return(defaultvalue);
}

void earth3d::setAttribute(QString attr, QString value) {
  // change attribute

  configAttributesMap[attr]=value;
}

void saveAttributes() {
  // save all attributes to the configuration file

  QDomDocument doc;

  QDomNode node = doc.createElement("earth3dconfig");
  doc.appendChild(node);

  QMap<QString, QString>::iterator it = configAttributesMap.begin();
  while(it != configAttributesMap.end()) {
    QDomElement attrnode = doc.createElement("attribute");
    attrnode.setAttribute("name", it.key());
    attrnode.setAttribute("value", it.data());

    node.appendChild(attrnode);

    it++;
  }

  QDir().mkpath(xmlconfigfilepath);
  QFile file(QDir(xmlconfigfilepath).filePath("earth3d.xml"));
  file.open(QIODevice::WriteOnly);
  QTextStream textstream(&file);
  textstream << doc.toString().latin1();
  file.close();
}

void loadCachedAttributes() {
  cacheUseMultiTexturing = getAttribute("usemultitexturing", "true")=="true";
  cacheUseTextureCompression = getAttribute("usetexturecompression", "true")=="true";
}
