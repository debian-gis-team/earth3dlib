#include "resources.h"
#include <q3dragobject.h>
#include <Q3MimeSourceFactory>
//Added by qt3to4:
#include <QPixmap>

QPixmap getPixmap(const char *name) {
  const QMimeSource* mime_source = Q3MimeSourceFactory::defaultFactory()->data( name );

  if ( mime_source == 0 ) return QPixmap();

  QPixmap pixmap;
  Q3ImageDrag::decode( mime_source, pixmap );
  return pixmap;
}
