#include "pngutils.h"
#include "jpgutils.h"
#include "mapTileTreeNodeCore.h"
#include "dataReceivedMapTile.h"
#include <assert.h>
#include <stdlib.h>

MapTileTreeNodeCore::MapTileTreeNodeCore() : RequestIDNode() {
  generated = false;
  requested = false;
  downloaded = false;
  downloadFailed = false;

  tile = NULL;

  createMapTile();
}

MapTileTreeNodeCore::~MapTileTreeNodeCore() {
  /* stop all downloads for this mapTile */
  stopDownloads();

#ifdef DEBUG
  printf("~MapTileTreeNodeCore\n");
#endif

  if (tile) {
    delete(tile);
    tile = NULL;
  }
}

void MapTileTreeNodeCore::addDownloader(StopableDownload *drl) {
  assert(runningDownloads<2);
  Downloadable::addDownloader(drl);
}

void MapTileTreeNodeCore::createMapTile() {
  tile = new MapTileUncompressed();
  tile->setBPP(getBPP());
}

void MapTileTreeNodeCore::setRequested(bool requested) {
  this->requested = requested;
}

bool MapTileTreeNodeCore::getRequested() {
  return(requested);
}
  
void MapTileTreeNodeCore::setGenerated(bool generated) {
  this->generated = generated;
}

bool MapTileTreeNodeCore::getGenerated() {
  return(generated);
}

void MapTileTreeNodeCore::setDownloaded(bool downloaded) {
  this->downloaded = downloaded;
}

bool MapTileTreeNodeCore::getDownloaded() {
  return(downloaded);
}

void MapTileTreeNodeCore::setDownloadFailed() {
  this->downloadFailed = true;
}

bool MapTileTreeNodeCore::getDownloadFailed() {
  return(downloadFailed);
}

void MapTileTreeNodeCore::setMapTile(MapTile &newtile) {
  tile->take(newtile);
}

void MapTileTreeNodeCore::setImage(int width, int height, const char *image, int size, const char *type) {
  tile->width = width;
  tile->height = height;
  tile->setImage(image, type, size);

  /* delete outdated version of uncompressedimage */
  tile->discardUncompressedImage();
}

int MapTileTreeNodeCore::getWidth() {
  return(tile->width);
}

int MapTileTreeNodeCore::getHeight() {
  return(tile->height);
}

bool MapTileTreeNodeCore::hasImage() {
  return(getCompressedImage()!=NULL);
}

char *MapTileTreeNodeCore::getUncompressedImage() {
//   QMutexLocker qml(&uncompressedimagemutex);
  return(tile->getUncompressedImage());
}

int MapTileTreeNodeCore::getUncompressedImageSize() {
  return(getWidth()*getHeight()*getBPP());
}

char *MapTileTreeNodeCore::getCompressedImage() {
  return(tile->image);
}

int MapTileTreeNodeCore::getCompressedImageSize() {
  return(tile->imagesize);
}

void MapTileTreeNodeCore::discardUncompressedImage() {
  QMutexLocker qml(&uncompressedimagemutex);
  tile->discardUncompressedImage();
}

void MapTileTreeNodeCore::removeMapTile() {
}

int MapTileTreeNodeCore::getBPP() {
  return(3);
}

char *MapTileTreeNodeCore::getScaledUncompressedImage() {
  return(tile->getScaledUncompressedImage());
}

int MapTileTreeNodeCore::getScaledWidth() {
  return(tile->getScaledWidth());
}

int MapTileTreeNodeCore::getScaledHeight() {
  return(tile->getScaledHeight());
}

void MapTileTreeNodeCore::garbageCollect() {
}

