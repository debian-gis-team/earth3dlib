#ifndef STATUSOBSERVER_H
#define STATUSOBSERVER_H

class StatusObserver {
 public:
  float memTexture, memUncompressed, memScaled, memVertex, memTexcoord, memUrlDownload;

  enum StatusObserverMemorySet { MEM_TEXTURE, MEM_UNCOMPRESSED, MEM_SCALED, MEM_VERTEX, MEM_TEXCOORD, MEM_URLDOWNLOAD };

  StatusObserver();
  virtual ~StatusObserver();

  virtual void changeMemoryOffset(enum StatusObserverMemorySet soms, float difference);
};

extern StatusObserver statusobserver;

#endif
