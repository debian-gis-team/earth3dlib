#include "requestIDNode.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

RequestIDNode::RequestIDNode() {
  requestID = NULL;
  magic = 45678;
  
  for(int nr=0; nr<4; nr++) childRequestIDs[nr]=NULL;
}

RequestIDNode::~RequestIDNode() {
#ifdef DEBUG
  printf("~RequestIDNode\n");
#endif
  if (requestID) delete[](requestID);
  
  for(int nr=0; nr<4; nr++) if (childRequestIDs[nr]) delete[](childRequestIDs[nr]);
}

void RequestIDNode::setRequestID(const char *requestID) {
  QMutexLocker qml(&requestIDmutex);

#ifdef DEBUG
  printf("setRequestID(%s)\n", requestID);
#endif
//   assert(requestID!=NULL);

  if (this->requestID) delete[](this->requestID);

  if (requestID) {
    this->requestID = new char[strlen(requestID)+1];
    strcpy(this->requestID, requestID);
  }
  else {
    this->requestID = NULL;
  }
}

char *RequestIDNode::getRequestID() {
  return(requestID);
}

void RequestIDNode::setChildRequestID(int nr, const char *requestID) {
  QMutexLocker qml(&requestIDmutex);

  if (this->childRequestIDs[nr]) {
    delete[](this->childRequestIDs[nr]);
    this->childRequestIDs[nr] = NULL;
  }
  if (requestID) {
    this->childRequestIDs[nr] = new char[strlen(requestID)+1];
    strcpy(this->childRequestIDs[nr], requestID);
  }
}

char *RequestIDNode::getChildRequestID(int nr) {
  return(childRequestIDs[nr]);
}

