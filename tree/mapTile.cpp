#include "mapTile.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "statusObserver.h"
#include "globalsettings.h"

MapTile::MapTile() {
  image = NULL;
  width = 0;
  height = 0;
  type = NULL;
}

MapTile::~MapTile() {
#ifdef DEBUG
  printf("~MapTile\n");
#endif
  freeImage();
}

void MapTile::setImage(const char *newimage, const char *type, int size) {
  freeImage();

  char *localimage = new char[size];
  statusobserver.changeMemoryOffset(StatusObserver::MEM_TEXTURE, size);

  imagesize = size;
  memcpy(localimage, newimage, imagesize);

//   printf("MapTile::setImage(%s)\n", type);

  this->type = new char[strlen(type)+1];
  statusobserver.changeMemoryOffset(StatusObserver::MEM_TEXTURE, strlen(type)+1);

  strcpy(this->type, type);

  image = localimage;

  needRedraw = true;
}

void MapTile::freeImage() {
  if (image) {
    statusobserver.changeMemoryOffset(StatusObserver::MEM_TEXTURE, -float(imagesize));
    delete[](image);
    image = NULL;
  }

  if (type) {
    statusobserver.changeMemoryOffset(StatusObserver::MEM_TEXTURE, -float(strlen(type))+1);
    delete[](type);
    type = NULL;  
  }
}

bool MapTile::hasImage() {
  return(image!=NULL);
}

void MapTile::take(MapTile &newtile) {
  freeImage();

  image = newtile.image;
  width = newtile.width;
  height = newtile.height;
  type = newtile.type;

  newtile.image = NULL;
  newtile.width = 0;
  newtile.height= 0;
  newtile.type = NULL;
}
