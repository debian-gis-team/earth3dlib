#ifndef TEXTURETILETREENODECORE_H
#define TEXTURETILETREENODECORE_H
#include "winconf.h"
#include "mapTileUncompressed.h"
#include "mapTileTreeNodeCore.h"
#include "point3d.h"
#include <qgl.h>

class TextureTreeNodeCore : public MapTileTreeNodeCore {
  /* remove the texture from the graphic card memory */
  void freeTexture();

  /* save a copy of the old image */
  GLuint newtextureid;
  bool newtextureBound;

  /* saves a scaled version of the image */
  char *scaledImage;
  int scaledWidth;
  int scaledHeight;

  MapTileUncompressed newtile; // used for interpolation
 public:
  GLuint textureid;
  bool textureBound;

  Point3D vertex[4];

  TextureTreeNodeCore();
  virtual ~TextureTreeNodeCore();

  virtual void setVertex(int nr, Point3D p);
  Point3D *getVertex(int nr);
  virtual void setImage(int width, int height, const char *image, int size, const char *type);
  virtual bool getThisInterpolates();
  virtual void finishInterpolation();

  virtual void discardUncompressedImage();

  int getInterpolationTextureID();
  char *getInterpolationTexture();
  int getInterpolationTextureSize();
  int getInterpolationTextureWidth();
  int getInterpolationTextureHeight();

  /** Garbage collect */
  virtual void garbageCollect();
};

#endif
