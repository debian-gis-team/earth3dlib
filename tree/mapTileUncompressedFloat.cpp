#include "mapTileUncompressedFloat.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "heightfieldconfig.h"

void MapTileUncompressedFloat::generateUncompressedImage() {
  MapTileUncompressed::generateUncompressedImage();
  if (!uncompressedimage) return;

  /* convert from 16-Bit PNG to float image */
  if (strcmp(type, "RAW")!=0) {
    signed short int *intimage = (signed short int *) uncompressedimage;
    assert(sizeof(short int)==2);
    
    float *floatimage = new float[uncompressedsize/2];
    int floatimagesize = (uncompressedsize/2)*sizeof(float);
    signed short int inti;
    
    for(int i=0; i<uncompressedsize/2; i++) {
      if (littleendian) {
	floatimage[i] = intimage[i];
      }
      else {
       ((unsigned char *) &inti)[0]=((intimage[i]&0x00FF));
       ((unsigned char *) &inti)[1]=((intimage[i]&0xFF00)>>8);
        floatimage[i]=(float) (inti);
      }
//       printf("i: %i float: %f\n", i, floatimage[i]);
    }
    
    delete[](uncompressedimage);
    uncompressedimage = (char *) floatimage;
    uncompressedsize = floatimagesize;
  }
}

void MapTileUncompressedFloat::generateScaledImage() {
  if (!scaledgenerated) {
    if (width==SOURCEWIDTH && height==SOURCEHEIGHT) {
      /* we do not need a scaled image */
      scaledWidth = width;
      scaledHeight = height;
      
      if (scaledImage) delete[](scaledImage);
      scaledImage = NULL;
    }
    else {
      scaledWidth = SOURCEWIDTH;
      scaledHeight = SOURCEHEIGHT;
      printf("POW float: %i %i\n", scaledWidth, scaledHeight);
      if (scaledImage) delete[](scaledImage);
      float *floatScaledImage = new float[scaledWidth*scaledHeight];
      scaledImage = (char *) floatScaledImage;
      
      float *image = (float *) getUncompressedImage();
      float sx = float(width)/scaledWidth;
      float sy = float(height)/scaledHeight;
      if (image) {
	for(int ny=0; ny<scaledHeight; ny++) {
	  for(int nx=0; nx<scaledWidth; nx++) {
	    floatScaledImage[(ny*scaledWidth+nx)]=image[int(int(ny*sy)*width+int(nx*sx))];
	  }
	}
      }
      else {
	for(int ny=0; ny<scaledHeight; ny++) {
	  for(int nx=0; nx<scaledWidth; nx++) {
	    floatScaledImage[(ny*scaledWidth+nx)] = 0;
	  }
	}
      }
    }

    scaledgenerated = true;
  }
}

MapTileUncompressedFloat::MapTileUncompressedFloat() : MapTileUncompressed() {
  bpp = sizeof(float);

  /* get endianess */
  littleendian = false;
  short int order = 1;
  char *corder = (char *) &order;
  if (corder[0]==1) { // little endian, we need to convert the data
    littleendian = true;
  }
}

MapTileUncompressedFloat::~MapTileUncompressedFloat() {
}

