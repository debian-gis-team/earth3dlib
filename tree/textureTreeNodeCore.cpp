#include "textureTreeNodeCore.h"
#include "mapTileTreeNode.h"

#define GL_COMPRESSED_RGB                       0x84ED

TextureTreeNodeCore::TextureTreeNodeCore()
  : MapTileTreeNodeCore() {
  vertex[0] = Point3D(0,0,0);
  vertex[1] = Point3D(1,0,0);
  vertex[2] = Point3D(1,1,0);
  vertex[3] = Point3D(0,1,0);

  textureid = 0;
  textureBound = false;

  newtextureid = 0;
  newtextureBound = false;

  scaledImage = NULL;

  newtile.setBPP(getBPP());
}

TextureTreeNodeCore::~TextureTreeNodeCore() {
#ifdef DEBUG
  printf("~TextureTreeNodeCore\n");
#endif
  if (textureBound) {
    freeTexture();
  }
}

void TextureTreeNodeCore::setVertex(int nr, Point3D p) {
  vertex[nr]=p;
}

Point3D *TextureTreeNodeCore::getVertex(int nr) {
  return(&vertex[nr]);
}

void TextureTreeNodeCore::setImage(int width, int height, const char *image, int size, const char *type) {
  /* we set the image if there was no image before. otherwise the new image is saved
     until either a new setImage call or until it is used for interpolation. */
  if (hasImage()) {
    /* save a copy of the new image to use it later */
    newtile.width = width;
    newtile.height = height;
    newtile.setImage(image, type, size);
    newtextureid = 0;
    newtextureBound = false;
  }
  else {
    /* there was no image before. The new image is set as main image */

    /* set the new image */
    MapTileTreeNodeCore::setImage(width, height, image, size, type);

    /* unbind the texture from the graphics hardware */
    freeTexture();
  }
}

void TextureTreeNodeCore::freeTexture() {
  if (textureBound) {
    glDeleteTextures(1, &textureid);
    textureid = 0;
    textureBound = false;
  }
  if (newtextureBound) {
    glDeleteTextures(1, &newtextureid);
    newtextureid = 0;
    newtextureBound = false;
  }
}

bool TextureTreeNodeCore::getThisInterpolates() {
  return(newtile.hasImage());
}

void TextureTreeNodeCore::finishInterpolation() {
  /* put newimage as main image in this core */
  if (newtile.hasImage()) {
    MapTileTreeNodeCore::setImage(newtile.width, newtile.height, newtile.image, newtile.imagesize, newtile.type);
    newtile.freeImage();
  }
}

void TextureTreeNodeCore::discardUncompressedImage() {
  MapTileTreeNodeCore::discardUncompressedImage();

  tile->discardUncompressedImage();
  tile->discardScaledImage();
}

int TextureTreeNodeCore::getInterpolationTextureID() {
  if (!newtextureBound && newtile.hasImage()) {
    glGenTextures(1, &newtextureid);
    glBindTexture(GL_TEXTURE_2D, newtextureid);   // 2d texture (x and y size)
    newtextureBound = true;

    /* if width or height is not 2^x, we need to rescale the image */
    char *image = newtile.getScaledUncompressedImage(); // calls generate if needed
#ifdef USE_GL_TEXTURE_COMPRESSION
    glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB, newtile.getScaledWidth(), newtile.getScaledHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, image);
#else
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, newtile.getScaledWidth(), newtile.getScaledHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, image);
#endif

    /* save some memory */
    newtile.discardUncompressedImage();
    newtile.discardScaledImage();
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);	// Linear Filtering
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);	// Linear Filtering
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP); // prevent wrap artifacts
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP); // prevent wrap artifacts
  }

  return(newtextureid);
}

int TextureTreeNodeCore::getInterpolationTextureWidth() {
  return(newtile.width);
}

int TextureTreeNodeCore::getInterpolationTextureHeight() {
  return(newtile.height);
}

char *TextureTreeNodeCore::getInterpolationTexture() {
  return(newtile.getUncompressedImage());
}

int TextureTreeNodeCore::getInterpolationTextureSize() {
  return(newtile.getUncompressedImageSize());
}

void TextureTreeNodeCore::garbageCollect() {
  MapTileTreeNodeCore::garbageCollect();

  discardUncompressedImage();
  freeTexture();
}

