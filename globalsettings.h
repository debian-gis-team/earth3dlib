#ifndef GLOBALSETTINGS_H
#define GLOBALSETTINGS_H
#include <QApplication>
#include "fileCache.h"

class FormView;

#ifdef WIN32
typedef double DOUBLE;
#else
#define DOUBLE double
#endif

#define GLDOUBLE GL_DOUBLE
#define GLVERTEX3D glVertex3f

extern float MAXTILESIZE;
extern float CENTERWEIGHT;
extern bool needRedraw;

/* static float MAXTILESIZE=0.4; */
#define MAXTILESIZE_2 (MAXTILESIZE/2.)

#ifdef DAVE
#define MINIMUMDEPTH 3
#else
#define MINIMUMDEPTH 2
#endif

#define MAXDEPTH 18

extern bool alwaysBind;
extern bool flyMode;
extern FormView *formview;

/** if this is set to true, no frame is skipped even if interaction get bad. Useful for capturing movies. */
extern bool frameByFrame;

/** 0=normal, 1=grid, 2=border */
extern int maptiledrawtype;

/* heightfield factor */
extern float heightfieldmultiplier;

/* concurrent downloads */
extern int concurrentdownloads;

#define POIFONTSIZE 12

extern QMap<QString, QString> configAttributesMap;

/* to prevent calling getAttributes once per frame, we cache two attributes here */
extern bool cacheUseMultiTexturing;
extern bool cacheUseTextureCompression;

/* local file cache */
extern FileCache fileCache;

// Functions to read the earth3d.xml configuration file from the main directory.

// path to the configuration directory
extern QString xmlconfigfilepath;

void readXMLConfig(QString path);
bool isAttributeExisting(QString attr);
QString getAttribute(QString attr);
QString getAttribute(QString attr, QString defaultvalue);
void setAttribute(QString attr, QString value);
void saveAttributes();
void loadCachedAttributes();

namespace earth3d {
  void setAttribute(QString attr, QString value);
};

void saveAttributes();

#endif
