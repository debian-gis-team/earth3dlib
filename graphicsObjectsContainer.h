#ifndef GRAPHICSOBJECTSCONTAINER_H
#define GRAPHICSOBJECTSCONTAINER_H
#include <QVector>
#include "draw.h"
#include <QMutex>
#include <QMap>

using namespace std;

/**
 * Contains a list of Draw objects that are displayed inside the Earth3dWidget. Objects
 * can also be found (optionally) by a string identifier.
 */
class GraphicsObjectsContainer {
  QVector<Draw *> objectList;
  QMutex objectListMutex;

  /** Let the objects find identical objects by using identifiers like "planet"
   */
  QMap<QString, Draw *> gocTypes;

 public:
  GraphicsObjectsContainer();
  ~GraphicsObjectsContainer();

  void add(Draw *td, QString *identifier=NULL, void *userdata=NULL);
  void remove(Draw *td);
  QVector<Draw *> *getList();
  Draw *getListElement(int nr);
  Draw *findListElementByIdentifier(QString identifier);
  int getListSize();
};

#endif

