#include <qapplication.h>
#include <QFrame>
#include <QTextEdit>
#include <QVBoxLayout>

#include "earth3dwidget.h"
#include "mousenavigator.h"

using namespace std;

/**
 * This is the entry point for the example application. It creates a simple frame and
 * embeds an Earth3d-Widget. It also adds mouse navigation to the Earth3d-Widget.
 */
int main( int argc, char ** argv ) {
  QApplication a(argc, argv);

#ifdef MYMACOS
  a.addLibraryPath(a.applicationDirPath() + "/../PlugIns");
#endif

  QFrame mainForm;
  Earth3dWidget earth(&mainForm);
  earth.setNavigator(new MouseNavigator(&earth));

  // Set a local or remote source for the data:

  // earth.setEarthSource("http://localhost/earth/earth/textures/0.mxml", "http://localhost/earth/earth/heightfield/0.mxml");
  // earth.setEarthSource("file:////earthlib/data/earth/textures/0.mxml", "file:////earthlib/data/earth/heightfield/0.mxml");
  earth.setEarthSource("http://earth3d.cgv.tugraz.at/bm_ng/january2004/texture/0.mxml", "http://earth3d.cgv.tugraz.at/earth/heightfield/0.mxml");

  QVBoxLayout vboxlayout;
  vboxlayout.addWidget(&earth);

  mainForm.setLayout(&vboxlayout);
  mainForm.setMinimumSize(QSize(640, 480));
  mainForm.show();

  return a.exec();
}
