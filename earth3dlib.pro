#
# See www.earth3d.org
#

QT += qt3support

win32 {
  INCLUDEPATH	+= H:\\Studium\\uEngine\\earth\\visualization\\QT\\libxml\\libxml2-2.6.6.win32\\include "C:\\include"
  LIBS	+= -LC:/lib -llibpng
  DESTDIR = .
#  DEFINES += WIN32_ZBUFFER_BUG
#  CONFIG += console
  DEFINES += GENERATE_FONTS
  SOURCES += draw/wingl.cpp
  RC_FILE = earthviewer.rc
}

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
!mac {
  DEFINES	+= LINUX
#  LIBS += -ljsw
#  QMAKE_CXXFLAGS_RELEASE += -march=pentium4 -O2 -pipe -fomit-frame-pointer
#  QMAKE_CXXFLAGS_RELEASE += -march=athlon-xp -O2 -pipe -fomit-frame-pointer
#  QMAKE_CXXFLAGS_RELEASE += -march=pentium3 -O2 -pipe -fomit-frame-pointer
#  QMAKE_CXXFLAGS_RELEASE += -mcpu=pentium -march=pentium -O2 -pipe -fomit-frame-pointer
}
}

mac {
  QMAKE_CXXFLAGS_RELEASE += -mcpu=7400 -maltivec
  DEFINES       += MYMACOS
#  DEFINES += GENERATE_FONTS
  RC_FILE = earth3d.icns
  LIBS += -lpng
}

# QMAKE_CXXFLAGS_DEBUG += -pg
# QMAKE_LFLAGS_DEBUG += -pg

# DEFINES     += DEPTH_STATISTIC

DEFINES += FORWARD_VIEW

DEFINES += QT_THREAD_SUPPORT
# DEFINES += FORWARD_VIEW_DRAW
DEFINES += USE_GL_TEXTURE_COMPRESSION
# DEFINES += SCREENSHOT_ALWAY_BIND_TEXTURE
# DEFINES += DEBUG_SPHERES
# # DEFINES += USE_ICON_BLENDING
# DEFINES += JOYSTICK
# DEFINES += ATMOSPHERE


# DEFINES	+= RELATIVETRANSFORM


TEMPLATE	= app
LANGUAGE	= C++

QMAKE_CXXFLAGS_DEBUG += -D EARTH3DDEBUG
CONFIG	+= qt thread opengl warn_on debug stl
RESOURCES=

# DEFINES	+= STATISTIC_NODECOUNT
INCLUDEPATH	+= /usr/include/libxml2/libxml /usr/include/libxml2 ../../network/rubyAgents/c ../../dataserver /sw/include /usr/X11R6/include/ /sw/include/libxml2 geometry/ network/ draw/ tree/ ./

HEADERS	+= earth3dwidget.h \ 
    network/qServiceLib.h \
	network/connectNetworkService.h \
	network/urlDownload.h \
        network/proxyUrlOperator.h \
	network/urlRawDownload.h \
        network/md5.h \
        network/fileCache.h \
	geometry/geometry2d3dSphere.h \
	geometry/geometry2d3d.h \
	draw/pngutils.h \
	globalsettings.h \
	draw/qGLWidgetDrawScene.h
SOURCES	+= main.cpp \
    earth3dwidget.cpp \
	draw/glcontext.cpp \
	globalsettings.cpp \
	resources.cpp \
	network/qServiceLib.cpp \
	network/urlDownload.cpp \
	network/downloadFinishedEvent.cpp \
	network/urlRawDownload.cpp \
	network/urlRawDownloadEvent.cpp \
	network/urlTools.cpp \
	network/connectNetworkServiceRequestQueue.cpp \
	network/connectNetworkServiceRequestQueueObject.cpp \
	network/connectNetworkService.cpp \
	network/stopableDownload.cpp \
	network/dataReceivedListener.cpp \
	network/dataReceivedMapTile.cpp \
	network/downloadable.cpp \
    network/proxyUrlOperator.cpp \
    network/fileCache.cpp \
    network/md5.cpp \
	tree/mapTile.cpp \
	tree/mapTileTree.cpp \
	tree/mapTileTreeNode.cpp \
	tree/mapTileTreeNodeCore.cpp \
	tree/heightfieldTree.cpp \
	tree/heightfieldTreeNode.cpp \
	tree/requestIDNode.cpp \
	tree/textureTreeNodeCore.cpp \
	draw/draw.cpp \
	draw/treeDraw.cpp \
	draw/treeDrawSphere.cpp \
	draw/treeDrawPOI.cpp \
	draw/treeDrawPOISign.cpp \
	draw/drawScene.cpp \
	draw/drawSceneObject.cpp \
	draw/drawSceneObjectTriangle.cpp \
	draw/drawSceneObjectSphere.cpp \
	draw/drawSceneObjectQuad.cpp \
	draw/drawSceneObjectTexture.cpp \
	draw/drawSceneObjectTransform.cpp \
	draw/drawSceneObjectTranslate.cpp \
	draw/drawSceneObjectScale.cpp \
	draw/drawSceneObjectRotatequaternion.cpp \
	draw/drawSceneObjectGroup.cpp \
	draw/drawSceneObjectUseGroup.cpp \
	draw/qGLWidgetDrawScene.cpp \
	draw/moveToPositionEvent.cpp \
	draw/imageList.cpp \
	graphicsObjectsContainer.cpp \
	tree/heightfieldTreeNodeCore.cpp \
	geometry/geometry2d3dFactory.cpp \
	geometry/geometry2d3dSphere.cpp \
	geometry/geometry2d3dplane.cpp \
	geometry/matrix.cpp \
	tree/mapTileUncompressed.cpp \
	tree/mapTileUncompressedFloat.cpp \
	tree/statusObserver.cpp \
	tree/mutex.cpp \
	tree/globaltimer.cpp \
	geometry/frustum.cpp \
	navigator.cpp \
    mousenavigator.cpp \
	geometry/quaternion.cpp \
	draw/pngutils.cpp \
	draw/jpgutils.cpp
FORMS= 

QT += xml opengl network
