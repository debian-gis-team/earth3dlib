#include "mousenavigator.h"
#include "earth3dwidget.h"
#include "geometry/quaternion.h"
#include "geometry/matrix.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/**
 * This navigator allows rotating and zooming the earth with the mouse.
 */
MouseNavigator::MouseNavigator(Earth3dWidget *earth) :
    Navigator(earth) {

}

MouseNavigator::~MouseNavigator() {

}

void MouseNavigator::mouseMoveEvent(QMouseEvent * e) {
    QMutexLocker qml(&navMutex);

    if (!earth->getDepthBuffer())
        return;

    if (e->state() & Qt::LeftButton && old_valid) {
        FloatImage *fi = earth->getDepthBuffer();
        
        float projplane = 0.3;
        int dy = (earth->size().height()-my);
        if (dy<0)
            dy=0;
        if (dy>=fi->height)
            dy=fi->height-1;
        if (mx>=fi->width)
            mx=fi->width-1;

        float depth1 = fi->buffer[dy*fi->width+mx];

        float omx = mx, omy = my;

        mx = e->x();
        my = e->y();

        dy = (earth->size().height()-my);
        if (dy<0)
            dy=0;
        if (dy>=fi->height)
            dy=fi->height-1;
        if (mx>=fi->width)
            mx=fi->width-1;

        /* get mouse position on screen */
        double sdx, sdy, sdz;
        float depth2 = fi->buffer[dy*fi->width+mx];

#ifdef DEBUG
        printf("clip: %f %f depth: %f %f\n", clippingPlanes[0], clippingPlanes[1], depth1, depth2);
#endif
        if (depth1>=earth->getClippingPlanes()[1]) {
            depth1 = depth2;
        }
        if (depth2>=earth->getClippingPlanes()[1]) {
            depth2 = depth1;
        }
        if (depth1>=earth->getClippingPlanes()[1] || depth2>=earth->getClippingPlanes()[1]) {
            depth1 = 0.2;
            depth2 = 0.2;
        }
        gluUnProject(omx, earth->size().height()-omy, depth1, earth->getModelviewMatrix(), earth->getProjectionMatrix(), earth->getViewport(), &old_sdx, &old_sdy, &old_sdz);

        //     gluUnProject(mx, size().height()-my, projplane, modl, proj, viewport, &sdx, &sdy, &sdz);
        gluUnProject(mx, earth->size().height()-my, depth2, earth->getModelviewMatrix(), earth->getProjectionMatrix(), earth->getViewport(), &sdx, &sdy, &sdz);

        Point3D oldmouse(old_sdx, old_sdy, old_sdz);
        Point3D newmouse(sdx, sdy, sdz);

        Point3D vector = newmouse-oldmouse;
        //     float len = sqrt((omx-mx)*(omx-mx)+(omy-my)*(omy-my))*1./(2*M_PI*oldmouse.length());
        float len = (vector.length()*360./(2*M_PI*oldmouse.length()));
        Point3D norm_oldmouse = oldmouse.normalize();
        Point3D norm_newmouse = newmouse.normalize();

#ifdef DEBUG
        printf("depth1: %f depth2: %f\n", depth1, depth2);
#endif

        float angle = acos(norm_oldmouse.skalarprodukt(norm_newmouse));
        vector = vector.normalize();
        Point3D axis = norm_oldmouse.crossproduct(norm_newmouse);
        axis = axis.normalize();

        //     axis = Point3D(0,1,0);
        float matrix[16];
        Quaternion quat;
#ifdef DEBUG
        printf("angle: %f\n", angle);
#endif
        quat.createFromAxisAngle(axis.x, axis.y, axis.z, len);
        quat.createMatrix(matrix);

        stopAutoNavigation();

        setViewer(Matrix::multvector(matrix, viewer));

        setDirection(Matrix::multvector(matrix, direction));
        up = Matrix::multvector(matrix, up);

#ifdef DEBUG
        printf("viewer: %f, %f, %f, %f\n", nav.viewer.x, nav.viewer.y, nav.viewer.z, nav.viewer.length());
#endif

        old_sdx = sdx;
        old_sdy = sdy;
        old_sdz = sdz;

        earth->updateGL();
    } else {
        if (e->state() & Qt::MidButton) {
            int omx = mx, omy = my;

            mx = e->x();
            my = e->y();

            /* rotate around */
            rotateOnSurface(mx-omx);
            elevate(omy-my);

            earth->updateGL();
        }
    }

    if (!old_valid) {
        if (earth->getMatrixValid()) {
            gluUnProject(earth->size().width()-mx, my, 0, earth->getModelviewMatrix(), earth->getProjectionMatrix(), earth->getViewport(), &old_sdx,
                    &old_sdy, &old_sdz);
            old_valid = true;
        }
    }
}

void MouseNavigator::mousePressEvent(QMouseEvent * e) {
    mx = e->x();
    my = e->y();
    old_valid = false;

#ifdef WIN32
    mousepressed = true;
#endif

    if (earth->getMatrixValid()) {
        gluUnProject(earth->size().width()-mx, my, 0, earth->getModelviewMatrix(), earth->getProjectionMatrix(), earth->getViewport(), &old_sdx, &old_sdy,
                &old_sdz);
        old_valid = true;
    }

    earth->setGrabDepthBuffer(true);
}

void MouseNavigator::mouseReleaseEvent(QMouseEvent * e) {
    old_valid = false;

#ifdef WIN32
    mousepressed=false;
#endif
}

void MouseNavigator::wheelEvent( QWheelEvent * e ) {
  float distance = float(e->delta()/float(120));
  distance /= 20;
  forward(distance);
  earth->updateGL();
}

