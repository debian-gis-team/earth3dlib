#ifndef FLOATIMAGE_
#define FLOATIMAGE_

/**
 * Contains an image with float pixels. It is used to save the Z-Buffer depth of the
 * 3D earth view. This information is used to know how far away the pixel was that was
 * clicked with the mouse.
 */
class FloatImage {
public:
    int width;
    int height;
    float *buffer;
};

#endif /*FLOATIMAGE_*/
