#ifndef GEOMETRY2D3DFACTORY
#define GEOMETRY2D3DFACTORY
#include "geometry2d3d.h"
#include <vector>

class Geometry2D3DFactory {
  std::vector<Geometry2D3D *> geoVector;

 protected:
  Geometry2D3DFactory();
  ~Geometry2D3DFactory();

 public:
  static Geometry2D3DFactory *getFactory();

  void registerGeometry(Geometry2D3D *geometry);
  Geometry2D3D *getGeometry(const char *type);
};

#endif
