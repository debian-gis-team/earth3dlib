#include <math.h>
#include "quaternion.h"

#ifndef M_PI
# define M_PI		3.14159265358979323846	/* pi */
#endif

Quaternion::Quaternion() {
  x = 0;
  y = 0;
  z = 0;
  w = 1;
}

void Quaternion::createFromAxisAngle(float x, float y, float z, float angle) 
{ 
  float rad_angle = (angle/180.0f) * M_PI;

  float sin_a = sin(rad_angle/2.);
  float cos_a = cos(rad_angle/2.);

  // Calculate the x, y and z of the quaternion
  this->x = x*sin_a;
  this->y = y*sin_a;
  this->z = z*sin_a;
  this->w = cos_a;
}


void Quaternion::createMatrix(float *matrix)
{
  if (!matrix) return;
  
  matrix[ 0] = 1.0f - 2.0f * ( y * y + z * z );  
  matrix[ 1] = 2.0f * ( x * y - w * z );  
  matrix[ 2] = 2.0f * ( x * z + w * y );  
  
  matrix[ 4] = 2.0f * ( x * y + w * z );  
  matrix[ 5] = 1.0f - 2.0f * ( x * x + z * z );  
  matrix[ 6] = 2.0f * ( y * z - w * x );  
  
  matrix[ 8] = 2.0f * ( x * z - w * y );  
  matrix[ 9] = 2.0f * ( y * z + w * x );  
  matrix[10] = 1.0f - 2.0f * ( x * x + y * y );  
  
  matrix[12] = 0;  
  matrix[13] = 0;  
  matrix[14] = 0;  
  
  matrix[ 3] = matrix[ 7] = matrix[11] = matrix[12] = matrix[13] = matrix[14] = 0.0f;
  matrix[15] = 1.0f;
}
