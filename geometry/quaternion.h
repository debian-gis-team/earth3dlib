#ifndef QUATERNION_H
#define QUATERNION_H

/* see matrfaq.html */

class Quaternion {
 public:
  Quaternion();
  
  void createFromAxisAngle(float x, float y, float z, float angle);
  void createMatrix(float *matrix);
  
 private:
  float x, y, z, w;
};

#endif
