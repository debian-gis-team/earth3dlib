#ifndef POINT3D_H
#define POINT3D_H
#include <math.h>
#include "globalsettings.h"

class Point3D {
 public:
  DOUBLE x,y,z;

  Point3D() : x(0), y(0), z(0) {
  };

  Point3D(DOUBLE vx, DOUBLE vy, DOUBLE vz) : x(vx), y(vy), z(vz) {
  };

  Point3D(const Point3D &p) : x(p.x), y(p.y), z(p.z) {
  };

  Point3D operator+(Point3D p2) {
    return(Point3D(x+p2.x, y+p2.y, z+p2.z));
  };

  Point3D operator+=(Point3D p2) {
    x+=p2.x;
    y+=p2.y;
    z+=p2.z;
    return(*this);
  };

  Point3D operator-=(Point3D p2) {
    x-=p2.x;
    y-=p2.y;
    z-=p2.z;
    return(*this);
  };

  Point3D operator-(Point3D p2) {
    return(Point3D(x-p2.x, y-p2.y, z-p2.z));
  };

  Point3D operator-() {
    return(Point3D(-x, -y, -z));
  };

  Point3D operator/(DOUBLE f) {
    return(Point3D(x/f, y/f, z/f));
  };

  Point3D operator*(DOUBLE f) {
    return(Point3D(x*f, y*f, z*f));
  };

  Point3D operator/=(DOUBLE f) {
    x/=f;
    y/=f;
    z/=f;
    return(*this);
  };

  Point3D operator*=(DOUBLE f) {
    x*=f;
    y*=f;
    z*=f;
    return(*this);
  };

  DOUBLE operator[](int i) {
    switch(i) {
    case 0:
      return(x);
    case 1:
      return(y);
    case 2:
      return(z);
    default:
      return(1);
    }
  };

  DOUBLE length() {
    return(sqrt(x*x+y*y+z*z));
  };

  bool operator==(Point3D a) {
    if (x==a.x && y==a.y && z==a.z) return(true);
    return(false);
  };

  Point3D normalize() {
    return(Point3D(x,y,z)/length());
  };

  DOUBLE skalarprodukt(Point3D &p) {
    return(x*p.x+y*p.y+z*p.z);
  };

  DOUBLE operator*(Point3D &p) {
    return(skalarprodukt(p));
  };

  DOUBLE skalarproduktRef(Point3D p) {
    return(x*p.x+y*p.y+z*p.z);
  };

  Point3D crossproduct(Point3D &b) {
    return(Point3D(y*b.z-z*b.y,
		   z*b.x-x*b.z,
		   x*b.y-y*b.x
		   ));
  };
};

#endif
