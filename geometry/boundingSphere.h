#ifndef BOUNDINGSPHERE
#define BOUNDINGSPHERE
#include "point3d.h"

class BoundingSphere {
 public:
  float radius;
  Point3D center;
};

#endif
