#ifndef GEOMETRY2D3DPLANE_H
#define GEOMETRY2D3DPLANE_H
#include "geometry2d3d.h"

class Geometry2D3DPlane : public Geometry2D3D {
 public:
  Geometry2D3DPlane();
  virtual ~Geometry2D3DPlane();

  virtual char *getType();

  virtual Point3D getPoint(Point2D p, float factor=1.f);
  /** This function does the inverse operation to getPoint. */
  virtual Point2D inverse(Point3D p);
};

#endif
