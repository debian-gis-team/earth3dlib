#ifndef POINT4D_H
#define POINT4D_H
#include "point3d.h"

class Point4D : public Point3D {
 public:
  float w;

  Point4D() {
    x = 0;
    y = 0;
    z = 0;
  };

  Point4D(float x, float y, float z, float w) 
    : Point3D(x,y,z) {
    this->w = w;
  };

  Point4D operator+(Point4D p2) {
    return(Point4D(x+p2.x, y+p2.y, z+p2.z, w+p2.w));
  };

  Point4D operator-(Point4D p2) {
    return(Point4D(x-p2.x, y-p2.y, z-p2.z, w-p2.w));
  };

  Point4D operator/(float f) {
    return(Point4D(x/f, y/f, z/f, w/f));
  };

  float operator[](int i) {
    switch(i) {
    case 0:
      return(x);
    case 1:
      return(y);
    case 2:
      return(z);
    case 3:
      return(w);
    default:
      return(1);
    }
  };

  float length() {
    return(sqrt(x*x+y*y+z*z+w*w));
  };
};

#endif
