#ifndef T_POINT2D_H
#define T_POINT2D_H

template <class datatype>
class T_Point2D {
 public:
  datatype x,y;

  T_Point2D() {
    x = 0;
    y = 0;
  };

  T_Point2D(datatype x, datatype y) {
    this->x = x;
    this->y = y;
  };

  T_Point2D operator+(T_Point2D p2) {
    return(T_Point2D(x+p2.x, y+p2.y));
  };

  T_Point2D operator/(double d) {
    return(T_Point2D(x/d, y/d));
  };
  
  T_Point2D operator-(T_Point2D p2) {
    return(T_Point2D(x-p2.x, y-p2.y));
  };

  T_Point2D operator*(datatype factor) {
    return(T_Point2D(factor*x, factor*y));
  };

  T_Point2D operator*=(datatype factor) {
    x *= factor;
    y *= factor;
    return(*this);
  };

  T_Point2D operator/=(datatype factor) {
    x /= factor;
    y /= factor;
    return(*this);
  };

  T_Point2D operator+=(T_Point2D p2) {
    x += p2.x;
    y += p2.y;
    return(*this);
  };
};

#endif
