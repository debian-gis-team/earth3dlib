#ifndef FRUSTUM_H
#define FRUSTUM_H

#ifdef WIN32
        #include "winconf.h"
	#include <windows.h>
	#include <GL/gl.h>
	#include <GL/glu.h>
	#include <math.h>
	#include <iostream>
#endif
#ifndef WIN32
	#include <math.h>
	#include <GL/gl.h>
	#include <GL/glu.h>
	#include <iostream>
#endif

// see http://www.sjbaker.org/steve/omniv/frustcull.html
// and http://www.flipcode.com/cgi-bin/msg.cgi?showThread=00010182&forum=3dtheory&id=-1

class CFrustum {
public:
  float   clip[16];
  
  void CalculateFrustum();

  bool PointInFrustum(float x, float y, float z);
  bool SphereInFrustum(float x, float y, float z, float radius);
private:
  float frustum[6][4];
};


#endif

