#include "geometry2d3dSphere.h"
#include "geometry2d3dFactory.h"
#include <assert.h>
#include <stdio.h>

#ifndef M_PI
# define M_PI		3.14159265358979323846	/* pi */
#endif

Geometry2D3DSphere geoSphere;

Geometry2D3DSphere::Geometry2D3DSphere() : Geometry2D3D() {
  Geometry2D3DFactory::getFactory()->registerGeometry(this);

  sintable = new DOUBLE[SINTABLESIZE];

  /* fill sintable */
  for(int i=0; i<SINTABLESIZE; i++) {
    sintable[i] = sin((M_PI*2/SINTABLESIZE)*i);
  }
}

Geometry2D3DSphere::~Geometry2D3DSphere() {
  delete[](sintable);
}

char *Geometry2D3DSphere::getType() {
  return("sphere");
}

inline DOUBLE Geometry2D3DSphere::getSin(DOUBLE x) {
  x+=10.;

  /* scale to full table size */
  x *= SINTABLESIZE;

  /* divide into integer and rest */
  int ix = int(x);
  x -= ix;
  ix = ix % SINTABLESIZE;

  /* get both table entries */
  DOUBLE ls = sintable[ix];
  DOUBLE rs = sintable[(ix+1) % SINTABLESIZE];

  /* interpolate */
  DOUBLE result = ls+(rs-ls)*x;

  return(result);
}

Point3D Geometry2D3DSphere::getPoint(Point2D p, float factor) {
  /*
    unassign('x','y');
    rotX := proc(a)
      matrix( [ [1,0,0], [0,cos(a),-sin(a)], [0,sin(a),cos(a)] ] )
    end;
    rotY := proc(a) 
      matrix( [ [cos(a),0,sin(a)], [0,1,0], [-sin(a),0,cos(a)] ] )
    end;
    rotZ := proc(a)
      matrix( [ [cos(a),-sin(a),0], [sin(a),cos(a),0], [0,0,1] ] )
    end;
    koor := proc(x,y)
      rotY(x)&*rotZ(y)&*vector([1,0,0])
    end;
    
    m1 := rotZ(x)&*rotY(y)&*vector([1,0,0]);
    m := evalm(m1);
    m2 := koor(x,y);
    evalm(m2);

    vector([cos(x)*cos(y), sin(x)*cos(y), -sin(y)])

    => x und y tauschen
   */
  DOUBLE radius = 1;
  int ix = int((p.x+10.) * SINTABLESIZE) % SINTABLESIZE;
//   assert(ix>=0 && ix<SINTABLESIZE);

  int iy = int(((-p.y + 0.5)*(SINTABLESIZE/2))+SINTABLESIZE)%SINTABLESIZE;
  DOUBLE fy = (p.y-0.5)*0.5;
  DOUBLE cosy = getSin(fy+5./4.);
  DOUBLE radiusfactor = radius * double(factor);

  return(Point3D(getSin(-p.x+5./4.)*cosy*radiusfactor, getSin(-fy)*radiusfactor, -getSin(p.x)*cosy*radiusfactor));
}

/* x = in.x*M_PI*2;
   y = (-in.y+0.5)*M_PI;

   p.x = cos(x)*cos(y)
   p.y = sin(y)
   p.z = -sin(x)*cos(y)

   result.x = acos(p.x/cos(y))
   result.y = asin(y)
   result.y = acos(p.z/-sin(x))
*/

Point2D Geometry2D3DSphere::inverse(Point3D p) {
  p = p.normalize();
  Point2D result;
  result.y = -asin(p.y)/M_PI+0.5;
  if (p.z<=0) {
    result.x = acos(p.x/cos(asin(p.y)))/(2*M_PI);
  }
  else {
    result.x = 1-acos(p.x/cos(asin(p.y)))/(2*M_PI);
  }

  return(result);
}
