#ifndef GEOMETRY2D3D_H
#define GEOMETRY2D3D_H
#include "point2d.h"
#include "point3d.h"

class Geometry2D3D {
 public:
  /** This function calculates a point in 3D space for a 2D object
   *  e.g. a heightfield.
   *  @param p a Point2D object that specifies the point in 2D space in a range from 0 to 1
   *  @param factor a float variable that moves the returned point normally orthogonal to the
   *         face, which is useful for heightfields, where this is used for the height of a
   *         point.
   *  @returns a point in 3D space, Point3D
   */
  virtual Point3D getPoint(Point2D p, float factor=1.f) = 0;

  virtual char *getType() = 0;

  /** This function does the inverse operation to getPoint. */
  virtual Point2D inverse(Point3D p) = 0;
};

#endif
