#include "geometry2d3dFactory.h"

Geometry2D3DFactory::Geometry2D3DFactory() {
}

Geometry2D3DFactory::~Geometry2D3DFactory() {
}

Geometry2D3DFactory *Geometry2D3DFactory::getFactory() {
  static Geometry2D3DFactory geoFactory;

  return(&geoFactory);
}

void Geometry2D3DFactory::registerGeometry(Geometry2D3D *geometry) {
  printf("registered %s\n", geometry->getType());
  geoVector.push_back(geometry);
  printf("size: %i\n", geoVector.size());
}

Geometry2D3D *Geometry2D3DFactory::getGeometry(const char *type) {
  printf("size: %i\n", geoVector.size());
  std::vector<Geometry2D3D *>::iterator i = geoVector.begin();
  while(i != geoVector.end()) {
    printf("found %s\n", (*i)->getType());
    if (strcmp((*i)->getType(), type)==0) {
      return(*i);
    }

    i++;
  }

  printf("Geometry type %s not found\n", type);
  abort();

  return(NULL);
}
