#ifndef DRAWSCENEOBJECTGROUP_H
#define DRAWSCENEOBJECTGROUP_H

#include "drawSceneObject.h"

class DrawSceneObjectGroup : public DrawSceneObject {
  QString groupName;

  bool generate_pbr;

  /* pbr data for this group */
  pbr_list *pbrlist;

 public:
  DrawSceneObjectGroup(DrawScene *scene);
  virtual ~DrawSceneObjectGroup();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Compiles this object and all of it children into the vector */
  virtual void compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);

  /** Compiles this object and all of it children into the vector */
  virtual void compileGroup(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);

  /** Set the calculated pbr data to be reused together with this group */
  virtual void setPBR(pbr_list *pbrlist);
};

#endif
