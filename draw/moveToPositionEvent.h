#ifndef MOVETOPOSITIONEVENT_H
#define MOVETOPOSITIONEVENT_H
#include <qevent.h>
#include "point3d.h"

class MoveToPositionEvent : public QEvent {
  QString name;
  Point3D mark, dir, up;

 public:
  MoveToPositionEvent(QString name, Point3D mark, Point3D dir, Point3D up);
  ~MoveToPositionEvent();

  QString getName();
  Point3D getMark();
  Point3D getDir();
  Point3D getUp();
};

#endif
