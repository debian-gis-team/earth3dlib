#include "drawSceneObjectGroup.h"
#include "drawSceneObjectUseGroup.h"

DrawSceneObjectUseGroup::DrawSceneObjectUseGroup(DrawScene *scene)
  : DrawSceneObject(scene) {
}

DrawSceneObjectUseGroup::~DrawSceneObjectUseGroup() {
}

void DrawSceneObjectUseGroup::readXMLData(QDomNode n) {
  registerXMLNames(n);

  /* n is the texture node */
  groupName = n.toElement().attribute("usename");

  /* parse children */
  DrawSceneObject::readXMLData(n);
}

void DrawSceneObjectUseGroup::compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* get group object */
  DrawSceneObjectGroup *group = (DrawSceneObjectGroup *) scene->getRegisteredDrawSceneObject(groupName);

  /* call this groups compile function instead */
  if (group) {
    group->compileGroup(targetlist, pbrlist);
  }
}
