#ifndef TREEDRAWSPHERE_H
#define TREEDRAWSPHERE_H

#include "winconf.h"
#include "treeDraw.h"
#include "rect2d.h"
#include "connectNetworkService.h"
#include "dataReceivedMapTile.h"
#include "geometry2d3d.h"
#include "point3d.h"
#include <qgl.h>
#include "frustum.h"
#include "heightfieldTree.h"
#include "textureTreeNodeCore.h"
#include <qmutex.h>
#include <qpixmap.h>

class TreeDrawSphere : public TreeDraw {
  /* Get the position on the surface that is important for the view (normally the position on the sphere that is
   * in the center of the screen.
   * @returns position on sphere or (0,0,0) if it does not intersect, the sphere is not visible at all or only a small part is visible
   */
  Point3D getVirtualViewer(const Point3D &direction, const Point3D &viewer);

  /** Select the nodes that should be drawn. */
  void checkNode(MapTileTreeNode *currentNode, vector<MapTileTreeNode *> *selectedNodes, Point3D *viewer, Point3D *virtualviewer, Rect2D nodeRect, GLfloat *projmodelmatrix, GLfloat *modelviewmatrix, int depth, float distsurface, Point3D direction);

  TextureTreeNodeCore *createTextureCore(MapTileTreeNode *currentNode, int childnr, Rect2D newNodeRect);
  HeightfieldTreeNodeCore *createHeightfieldCore(MapTileTreeNode *currentNode, int childnr, Rect2D newNodeRect, Point3D *vertex);

  bool checkVisible(Point3D *viewer, GLfloat *projmodelmatrix, GLfloat *modelviewmatrix, MapTileTreeNode *node, int depth, float distance);

  DOUBLE distanceFunction(DOUBLE surfacedistance, DOUBLE distance);
  DOUBLE getDistance(Point3D *p, Rect2D *rect);

  DOUBLE getDistance(const Point3D *p, Point3D rect[]);
  float getMaxEdgeSize(const Point3D *p, Point3D rect[], DOUBLE distance);

  /** Select the node for drawing */
  void selectNode(MapTileTreeNode *currentNode, vector<MapTileTreeNode *> *selectedNodes, bool visible);

  /** Request a node from the network using the DataReceivedMapTile object */
  void request(MapTileTreeNodeCore *core);

  /** Request a node from the heightfield tree. The heigtfield tree either scales the data
   *  it already has or requests new data.
   */
  void requestHeightfield(MapTileTreeNodeCore *core);

  /** This tree contains the available heightmap data in the tile size supported by the server.
   *  It can scale the map tiles to other sizes for use with the texture tiles.
   */
  HeightfieldTree *htree;
  
  /** The default heightfield when no other is available */
  float defaultHeightfield[32*32];
  
  ConnectNetworkService *cns;

  virtual Point3D getPointOnSphere(Point2D p);

  void bindTexture(TextureTreeNodeCore *core);

  void bindSingleGLTexture(int textureid, int width, int height, char *image);

  virtual void drawNodes(vector<MapTileTreeNode *> *selectedNodes, bool test);

  virtual void checkHeightfields(vector<MapTileTreeNode *> *selectedNodes);

  virtual void clearWasSelected(vector<MapTileTreeNode *> *wasSelectedNodes);

  virtual void clearSelections(vector<MapTileTreeNode *> *selectedNodes);

  virtual Rect2D getChildRect(int childnr, Rect2D &nodeRect);

  Geometry2D3D *gSphere;

  vector<MapTileTreeNode *> *wasSelectedNodes;

  CFrustum frustum;

#ifdef ATMOSPHERE
  QPixmap atmospherePixmap;
#endif
  bool viewatmosphere;

  unsigned int atmosphereTexture;
  void drawQuad(Point3D a, Point3D b);

 public:
  TreeDrawSphere(MapTileTree *mtt, ConnectNetworkService *cns, const char *geoType, bool viewatmosphere);
  virtual ~TreeDrawSphere();
  virtual void draw(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, int phase);
  virtual char *getType();

  virtual QImage getTextureAtCoordinates(DOUBLE x, DOUBLE y);
};

#endif
