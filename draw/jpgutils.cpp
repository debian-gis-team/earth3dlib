#include "jpgutils.h"
#include <qcolor.h>
#include <qbuffer.h>
#include <q3cstring.h>
#include <assert.h>

namespace JPGUtils {

  /** Read a JPG file from the source buffer to the destination buffer */
  void readJPG(char *source, int sourcesize, char *destination, int destinationsize) {
//     QByteArray sourceArray;
//     sourceArray.duplicate((const char *) source, (unsigned char) sourcesize);
    QImage image;
    image.loadFromData((unsigned char *) source, sourcesize, "JPEG");

//    image.convertDepth(32);
   int imageheight = image.height();
   int imagewidth = image.width();
    for(int row=0; row<imageheight; row++) {
      QRgb *p = (QRgb *)image.scanLine(row);
      for(int col=0; col<imagewidth; col++) {
	QRgb color = *(p + col);
	assert((row*imagewidth+col)*3+2<destinationsize);
#ifdef LINUX
	destination[(row*imagewidth+col)*3+0]=((unsigned char *) &color)[2]; //qRed(color);
	destination[(row*imagewidth+col)*3+1]=((unsigned char *) &color)[1]; //qGreen(color);
	destination[(row*imagewidth+col)*3+2]=((unsigned char *) &color)[0]; //qBlue(color);
#else
	destination[(row*imagewidth+col)*3+0]=qRed(color);
	destination[(row*imagewidth+col)*3+1]=qGreen(color);
	destination[(row*imagewidth+col)*3+2]=qBlue(color);
#endif
      }
    }
  }

  /** Write a JPG file. It uses the image from source, width, height, depth
   *  to write the image into the buffer destination. The size of the JPG image
   *  is returned in destinationsize. In the beginning, destinationsize must
q   *  be the size of the allocated buffer in destination.
   */
  void writeJPG(char *source, int width, int height, int depth, char *destination, int *destinationsize, int quality) {
    QImage image( width, height, 32 );
    for(int row=0; row<height; row++) {
      QRgb *p = (QRgb *)image.scanLine(row);
      for(int col=0; col<width; col++) {
	QRgb *color = p + col;
        *color = qRgb(source[(row*width+col)*3+0], source[(row*width+col)*3+1], source[(row*width+col)*3+2]);
      }
    }
//    QImage image( (unsigned char *) source, width, height, depth, NULL, 3, QImage::IgnoreEndian);

    QByteArray ba;
    QBuffer buffer( &ba );
    buffer.open( QIODevice::WriteOnly );
    image.save( &buffer, "JPEG", quality ); // writes image into ba in JPG format
    buffer.close();
    memcpy(destination, ba.data(), ba.size());
    *destinationsize = ba.size();
  }

}
