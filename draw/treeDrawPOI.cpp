#define GL_GLEXT_PROTOTYPES
#include "treeDrawPOI.h"
#include <stdio.h>
#include <GL/glext.h>
#include "treeDrawPOISign.h"
//Added by qt3to4:
#include <QMouseEvent>

TreeDrawPOI::TreeDrawPOI() {
  printf("================================================ TreeDrawPOI ===================================================\n");

#ifndef MYMACOS
  this->font.setPointSize(POIFONTSIZE);
#endif
}

TreeDrawPOI::~TreeDrawPOI() {
  clear();
}

void TreeDrawPOI::addObject(TreeDrawPOIObject *object) {
  QMutexLocker qml(&tdpoiListMutex);

  tdpoiList.push_back(object);
}

void TreeDrawPOI::draw(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, int phase) {
  if (phase!=2) return;

  QMutexLocker qml(&tdpoiListMutex);

#ifndef WIN32
  glActiveTextureARB(GL_TEXTURE1_ARB);
  glClientActiveTextureARB(GL_TEXTURE1_ARB);
  glDisable(GL_TEXTURE_2D);                    // Disable texture mapping.
  glDisable(GL_LIGHTING);

  glActiveTextureARB(GL_TEXTURE0_ARB);
  glClientActiveTextureARB(GL_TEXTURE0_ARB);
  glDisable(GL_TEXTURE_2D);                    // Disable texture mapping.
  glDisable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);
#endif

  /* save matrices for double click events */
  glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);
  glGetIntegerv(GL_VIEWPORT, viewport);

//   glPointSize(5);

  std::vector<TreeDrawPOIObject *>::iterator i;
  for (i = tdpoiList.begin(); i != tdpoiList.end(); i++) {
    (*i)->draw(viewer, viewculling, widget, this);
  };
}

void TreeDrawPOI::clear() {
  QMutexLocker qml(&tdpoiListMutex);

  std::vector<TreeDrawPOIObject *>::iterator i;
  for (i = tdpoiList.begin(); i != tdpoiList.end(); i++) {
    delete(*i);
  };
  tdpoiList.clear();
}

Draw *TreeDrawPOI::tagGeometry(QDomNode geometryNode, Draw *newDraw) {
  /* create new point of interest drawer */
  QDomElement geometryElement = geometryNode.toElement();
  QString base = geometryElement.attribute("base", QString("sphere"));

  TreeDrawPOI *tdpoi;
  if (newDraw) {
    tdpoi = (TreeDrawPOI *) newDraw;

    QString geometryMethod = geometryElement.attribute("method");
    if (geometryMethod == QString("replace")) {
      /* replace with the new data */
      tdpoi->clear();
    }
    else {
      /* otherwise add the new data */
    }
  }
  else {
    tdpoi = new TreeDrawPOI();
  }

  tdpoi->parseXML(geometryNode.firstChild(), base);

  /* add point of interest drawer to GraphicsObjectsContainer */
  return(tdpoi);
}

void TreeDrawPOI::parseXML(QDomNode n3, QString base) {
  /* fill in the data */
  while( !n3.isNull() ) {
    if (n3.isElement() && (n3.toElement().tagName() == QString("sign"))) {
      QDomElement signElement = n3.toElement();
      QString signName = signElement.attribute("name");
      float signX = atof(signElement.attribute("x").latin1());
      float signY = atof(signElement.attribute("y").latin1());
      float signZ = atof(signElement.attribute("z").latin1());

      float r = atof(signElement.attribute("r","1.").latin1());
      float g = atof(signElement.attribute("g","0.5").latin1());
      float b = atof(signElement.attribute("b","0").latin1());

      QString iconname = signElement.attribute("icon");
      QString url = signElement.attribute("url");
      float distance = atof(signElement.attribute("distance","100000").latin1());

      printf("iconname: %s distance: %f\n", iconname.latin1(), distance);
      /* insert the core */
      addObject(new TreeDrawPOISign(signName, Point3D(signX, signY, signZ), base.latin1(), &il, iconname, url, distance, r, g, b));
    }
    
    n3 = n3.nextSibling();
  }
}

bool TreeDrawPOI::mouseDoubleClickEvent(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, QMouseEvent *e) {
  bool done = false;
  std::vector<TreeDrawPOIObject *>::iterator i;
  for (i = tdpoiList.begin(); i != tdpoiList.end(); i++) {
    done = (*i)->mouseDoubleClickEvent(viewer, viewculling, widget, direction, e, this);
    if (done) break;
  }

  return(done);
}

