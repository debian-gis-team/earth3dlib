#ifndef DRAWSCENEOBJECTROTATEQUATERNION_H
#define DRAWSCENEOBJECTROTATEQUATERNION_H

#include "drawSceneObjectTransform.h"
#include "point3d.h"
#include "quaternion.h"
#include "matrix.h"

class DrawSceneObjectRotatequaternion : public DrawSceneObjectTransform {
  Point3D axis;
  float angle;

  float matrix[16];
  Quaternion quat;

 public:
  DrawSceneObjectRotatequaternion(DrawScene *scene);
  virtual ~DrawSceneObjectRotatequaternion();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Modify single point */
  virtual Point3D transformPoint3D(Point3D &p);
};

#endif
