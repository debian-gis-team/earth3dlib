#include "drawSceneObjectRotatequaternion.h"

DrawSceneObjectRotatequaternion::DrawSceneObjectRotatequaternion(DrawScene *scene)
  : DrawSceneObjectTransform(scene) {
}

DrawSceneObjectRotatequaternion::~DrawSceneObjectRotatequaternion() {
}

void DrawSceneObjectRotatequaternion::readXMLData(QDomNode n) {
  /* n is the texture node */
  axis.x = atof(n.toElement().attribute("x"));
  axis.y = atof(n.toElement().attribute("y"));
  axis.z = atof(n.toElement().attribute("z"));
  angle = atof(n.toElement().attribute("r"));

  /* parse children */
  DrawSceneObject::readXMLData(n);

  /* create Quaternion */
  quat.createFromAxisAngle(axis.x, axis.y, axis.z, angle);
  quat.createMatrix(matrix);
}

Point3D DrawSceneObjectRotatequaternion::transformPoint3D(Point3D &p) {
  return(Matrix::multvector(matrix, p));
}
