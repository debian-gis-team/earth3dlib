#ifndef TREEDRAWPOI_H
#define TREEDRAWPOI_H
#include "treeDraw.h"
#include "treeDrawPOIObject.h"
#include <vector>
#include <qmutex.h>
#include <qdom.h>
#include "imageList.h"

class TreeDrawPOI : public Draw {
  std::vector<TreeDrawPOIObject *> tdpoiList;
  QMutex tdpoiListMutex;
  ImageList il;
  void addObject(TreeDrawPOIObject *object);
 public:
  double modelMatrix[16], projMatrix[16];
  GLint viewport[4];
  QFont font;

  TreeDrawPOI();
  virtual ~TreeDrawPOI();

  static Draw *tagGeometry(QDomNode geometryNode, Draw *newdraw);

  void parseXML(QDomNode n3, QString base);

  virtual void draw(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, int phase);

  virtual void clear();

  virtual bool mouseDoubleClickEvent(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, QMouseEvent *e);
};

#endif
