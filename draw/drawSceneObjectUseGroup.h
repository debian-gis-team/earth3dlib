#ifndef DRAWSCENEOBJECTUSEGROUP_H
#define DRAWSCENEOBJECTUSEGROUP_H

#include "drawSceneObject.h"

class DrawSceneObjectUseGroup : public DrawSceneObject {
  QString groupName;

 public:
  DrawSceneObjectUseGroup(DrawScene *scene);
  virtual ~DrawSceneObjectUseGroup();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Compiles this object and all of it children into the vector */
  virtual void compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);
};

#endif
