#ifndef DRAWSCENEOBJECTSCALE_H
#define DRAWSCENEOBJECTSCALE_H

#include "drawSceneObjectTransform.h"
#include "point3d.h"

class DrawSceneObjectScale : public DrawSceneObjectTransform {
  Point3D scale;

 public:
  DrawSceneObjectScale(DrawScene *scene);
  virtual ~DrawSceneObjectScale();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Modify single point */
  virtual Point3D transformPoint3D(Point3D &p);
};

#endif
