#ifndef IMAGELIST_H
#define IMAGELIST_H
#include <qimage.h>
#include <qmap.h>
#include "dataReceivedListener.h"

class ImageList : public DataReceivedListener {
  QMap<QString, QImage> imageTable;

  void downloadIcon(QString &iconname);

 public:
  ImageList();

  QImage *getImage(QString &iconname);

  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);
};

#endif
