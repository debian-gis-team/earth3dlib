#include "treeDrawPOISign.h"
#include "earth.h"
#include <string.h>
#include "geometry2d3dFactory.h"
#include "globalsettings.h"
#include <qimage.h>
//Added by qt3to4:
#include <QMouseEvent>
#include "treeDrawPOI.h"
#ifndef WIN32
#include <unistd.h>
#endif

#define ICONSIZE 0.002


TreeDrawPOISign::TreeDrawPOISign(QString name, Point3D pos, const char *geoType, ImageList *il, QString iconname, QString url, float visibleDistance, float colorR, float colorG, float colorB) {
  this->il = il;
  this->iconname = iconname;
  this->url = url;
  this->visibleDistance = visibleDistance;
  this->colorR = colorR;
  this->colorG = colorG;
  this->colorB = colorB;

  if (name!=QString("")) {
    this->name = name;
  }
  else {
    name = "";
  }

  this->pos = pos;

  this->sphere = Geometry2D3DFactory::getFactory()->getGeometry(geoType);

  actionradius = 0;

#ifdef USE_ICON_BLENDING
  blendfactor = 0.f;
  firstdraw = true;
#endif
  
//   printf("addobject name=%s x=%f y=%f\n", name, pos.x, pos.y);
}

TreeDrawPOISign::~TreeDrawPOISign() {
}

void TreeDrawPOISign::draw(Point3D *viewer, bool viewculling, QGLWidget *widget, TreeDrawPOI *poi) {
  glPushMatrix();

  /* move to the right coordinates */
  Point2D p2(pos.x/360.+0.5, -pos.y/180.+0.5);
  Point3D p = sphere->getPoint(p2, 1+(pos.z*1./EARTHRADIUS));
//   glTranslatef(p.x, p.y, p.z);

//   /* visibility test */
//   GLfloat fbuffer[4];
//   glFeedbackBuffer(4, GL_3D, fbuffer);
//   glRenderMode(GL_FEEDBACK);
//   glBegin(GL_POINTS);
//   glVertex3f(0,0,0);
//   glEnd();
//   int count = glRenderMode(GL_RENDER);

  /* draw POI */  
  glColor3f(colorR, colorG, colorB);
  glDisable(GL_TEXTURE_2D);                    // Disable texture mapping.
  glDisable(GL_COLOR_MATERIAL);
  glDisable(GL_LIGHTING);

  /* get position for text */
  ptext = p;
//   ptext.normalize();
//   ptext *= (float) ICONSIZE*1.5;
//   ptext += p;

  //  glutSolidSphere(ICONSIZE, 10, 10);

//   printf("====================> skalarprodukt: %f\n", viewer->skalarprodukt(p));

  /* max view angle */
  float mviewa = acos(1./viewer->length());

  Point3D p3 = p.normalize();
  float skalar = viewer->normalize().skalarprodukt(p3);
  float a = acos(skalar);

//   double modelMatrix[16], projMatrix[16];
//   int viewport[4];
//   glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
//   glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);
//   glGetIntegerv(GL_VIEWPORT, viewport);
//   double winx, winy, winz;
//   gluProject(ptext.x, ptext.y, ptext.z, modelMatrix, projMatrix, viewport, &winx, &winy, &winz);
  float distance = (*viewer-p).length();
  distance *= REALEARTHRADIUS;

//   printf("==================> distance: %f\n", distance);

  if (a<mviewa && distance<visibleDistance) {
    visible = true;

    /* draw icon */
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//     glColor4f(1.f, 1.f, 1.f, blendfactor);
//     glEnable(GL_COLOR_MATERIAL);
 
//     printf("icon: %s\n", iconname.latin1());
    QImage *tex2 = il->getImage(iconname);

    GLfloat range[2];

    if (tex2) {
      /* draw text and icon ALWAYS ON TOP OF THE SCENE */
      glGetFloatv(GL_DEPTH_RANGE, range);
      glDepthRange(0,0);
      glDepthFunc(GL_ALWAYS);

      glRasterPos3f(ptext.x, ptext.y, ptext.z);
//       float textpos[2];
//       glGetFloatv(GL_CURRENT_RASTER_POSITION, textpos);
//       printf("textpos= %f %f\n", textpos[0], textpos[1]);
      actionradius = tex2->width()/2;

#ifdef USE_ICON_BLENDING
      if (firstdraw) blendfactor=1;
      firstdraw = false;

      if (blendfactor<1) {
	glPixelTransferf(GL_ALPHA_SCALE, blendfactor);
	blendfactor+=0.04;
	glDrawPixels( tex2->width(), tex2->height(), GL_RGBA, GL_UNSIGNED_BYTE, tex2->bits() );
	glPixelTransferf(GL_ALPHA_SCALE, 1.);
	needRedraw = true;
      }
      else {
	glDrawPixels( tex2->width(), tex2->height(), GL_RGBA, GL_UNSIGNED_BYTE, tex2->bits() );
      }
#else
      glDrawPixels( tex2->width(), tex2->height(), GL_RGBA, GL_UNSIGNED_BYTE, tex2->bits() );
#endif

//       ptext.x += tex2->width();
    }
    else {
      /* put line from center to bottom line of text */
      glBegin(GL_LINES);
      glVertex3f(ptext.x, ptext.y, ptext.z);
      glVertex3f(0,0,0);
//   glVertex3f(-p.x, -p.y, -p.z);
      glEnd();

      /* draw text and icon ALWAYS ON TOP OF THE SCENE */
      glGetFloatv(GL_DEPTH_RANGE, range);
      glDepthRange(0,0);
      glDepthFunc(GL_ALWAYS);
    }

    /* draw text */
//     glDisable(GL_DEPTH_TEST);
    if (widget) {
      double winx, winy, winz, tx, ty, tz;
      gluProject(ptext.x, ptext.y, ptext.z, poi->modelMatrix, poi->projMatrix, poi->viewport, &winx, &winy, &winz);
      winy -= 10;
      gluUnProject(winx, winy, winz, poi->modelMatrix, poi->projMatrix, poi->viewport, &tx, &ty, &tz);
//       widget->renderText(ptext.x, ptext.y, ptext.z, name, poi->font);
//       winy = poi->viewport[3]-winy;
      widget->renderText(tx, ty, tz, name, poi->font);
    }
//     glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthRange(range[0],range[1]);
  }
  else {
    visible = false;
#ifdef USE_ICON_BLENDING
    blendfactor = 0;
#endif
  }

  /* move back */
  glPopMatrix();
}

bool TreeDrawPOISign::mouseDoubleClickEvent(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, QMouseEvent *e, TreeDrawPOI *poi) {
  double winx, winy, winz;
  gluProject(ptext.x, ptext.y, ptext.z, poi->modelMatrix, poi->projMatrix, poi->viewport, &winx, &winy, &winz);
  float dx = e->x() - (winx+actionradius);
  float dy = e->y() - (poi->viewport[3]-winy-actionradius);
  float dist = (dx*dx)+(dy*dy);

  if (visible && dist<actionradius*actionradius && !url.isNull()) {
    // do some action
    printf("win: %f, %f, %f dist: %f name: %s\n", winx, winy, winz, dist, name);
    char command[5][1100], tmp[1100];

    // open url
    if (url.length()<1000) {
#ifdef LINUX
      strcpy(command[0], "mozilla");
      strcpy(command[1], "-remote");
      sprintf(tmp, "openURL(%s)", url.latin1());
      strcpy(command[2], tmp);
      printf("mozilla %s %s\n", command[1], command[2]);
      if (fork()==0) {
        execlp("mozilla", "mozilla", command[1], command[2], NULL);
      }
#endif
#ifdef MYMACOS
      if (fork()==0) {
        sprintf(tmp, "open %s", url.latin1());
        printf("%s with Mac's default browser\n", tmp);
        system(tmp);
      }
#endif
    }

    return(true);
  }

  return(false);
}
