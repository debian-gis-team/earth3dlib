#ifndef TREEDRAWPOISIGN_H
#define TREEDRAWPOISIGN_H
#include "treeDrawPOIObject.h"
#include "point3d.h"
#include "geometry2d3d.h"
#include <qfont.h>
#include "imageList.h"

class TreeDrawPOISign : public TreeDrawPOIObject {
  QString name;
  Point3D pos;
  Geometry2D3D *sphere;
  ImageList *il;
  QString iconname;
  Point3D ptext;
  int actionradius;
  bool visible;
  QString url;

  /* blend the flags */
  float blendfactor;
  /* let them pop up when they are visible at the first time drawn, otherwise
     it gets to slow */
  bool firstdraw;
  
  /* the color of the label */
  float colorR, colorG, colorB;

  float visibleDistance;

 public:
  TreeDrawPOISign(QString name, Point3D pos, const char *geoType, ImageList *il, QString iconname, QString url, float visibleDistance, float colorR, float colorG, float colorB);
  ~TreeDrawPOISign();

  virtual void draw(Point3D *viewer, bool viewculling, QGLWidget *widget, TreeDrawPOI *poi);

  virtual bool mouseDoubleClickEvent(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, QMouseEvent *e, TreeDrawPOI *poi);
};

#endif

