#ifndef DRAWSCENEOBJECTSPHERE_H
#define DRAWSCENEOBJECTSPHERE_H

#include "drawSceneObject.h"
#include "drawSceneObjectTexture.h"
#include <qdom.h>
#include "point3d.h"
#include "geometry2d3dSphere.h"

class DrawSceneObjectSphere : public DrawSceneObject {
  Point3D centervertex;
  double radius;
  int slices, stacks;
  QString texturename;
  Geometry2D3DSphere sphere;

  openglarray triangleopenglarray;

 public:
  DrawSceneObjectSphere(DrawScene *scene);
  virtual ~DrawSceneObjectSphere();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Compiles this object and all of it children into the vector */
  virtual void compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);
};

#endif
