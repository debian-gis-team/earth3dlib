#ifndef JPGUTILS_H
#define JPGUTILS_H
#include <qimage.h>

namespace JPGUtils {
  /** Read a JPG file from the source buffer to the destination buffer */
  void readJPG(char *source, int sourcesize, char *destination, int destinationsize);

  /** Write a JPG file. It uses the image from source, width, height, depth
   *  to write the image into the buffer destination. The size of the JPG image
   *  is returned in destinationsize. In the beginning, destinationsize must
   *  be the size of the allocated buffer in destination.
   */
  void writeJPG(char *source, int width, int height, int depth, char *destination, int *destinationsize, int quality=-1);
};

#endif
