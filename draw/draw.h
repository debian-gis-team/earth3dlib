#ifndef DRAW_H
#define DRAW_H
#include "winconf.h"
#include "point3d.h"
#include "draw.h"
#include <qgl.h>
#include <qstring.h>

class Draw {
  QString name;

 public:
  virtual ~Draw();
  /** Draw this object. Drawing is done in 4 phases (0-3) to draw 4 layers. 0 is used for drawing the background, like
   *  a skybox. 1 is used for normal objects like the earth object. 2 is used for front displays
   *  in the normal projection and 3 is used for head up displays with an orthogonal projection.
   */
  virtual void draw(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, int layer) = 0;
  virtual char *getType();
  virtual void setName(QString name);
  virtual QString getName();

  virtual bool mouseDoubleClickEvent(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, QMouseEvent *e);
};

#endif
