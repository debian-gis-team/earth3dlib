#ifndef DRAWSCENEOBJECTTEXTURE_H
#define DRAWSCENEOBJECTTEXTURE_H
#include "winconf.h"
#include <qgl.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qurl.h>
#include "urlDownload.h"
#include "dataReceivedListener.h"
#include "drawSceneObject.h"

class DrawSceneObjectTexture : public DrawSceneObject, DataReceivedListener {
  bool textureBound;
  GLuint textureID;
  QImage texture;
  QString name;
  QString url;

  /* text object */
  int textsize;
  QString text;
  QColor textcolor;

 protected:
  virtual void parseFilters(QDomNode n);

 public:
  DrawSceneObjectTexture(DrawScene *scene);
  virtual ~DrawSceneObjectTexture();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);  

  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);

  GLuint getTextureID(bool forgetCreation);

  virtual bool hasAlpha();
};

#endif
