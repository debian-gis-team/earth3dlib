#include "drawSceneObjectTexture.h"
#include "urlRawDownload.h"
#include "urlRawDownloadEvent.h"
#include <stdio.h>
#include <qpainter.h>
//Added by qt3to4:
#include <QPixmap>

#define GL_GLEXT_PROTOTYPES
#include <GL/glext.h>

DrawSceneObjectTexture::DrawSceneObjectTexture(DrawScene *scene) 
  : DrawSceneObject(scene) {
  textureBound = false;
//   texture = NULL;
  text = "";
  textcolor = Qt::black;
}

DrawSceneObjectTexture::~DrawSceneObjectTexture() {
//   if (texture) delete(texture);
}

/** Read its attributes from the XML document */
void DrawSceneObjectTexture::readXMLData(QDomNode n) {
  registerXMLNames(n);

  /* n is the texture node */
  name = n.toElement().attribute("name");
  url = n.toElement().attribute("url");
  bool allowCache = n.toElement().attribute("allowCache", "true")=="true";

  /* add filters/content to the texture */
  parseFilters(n.firstChild());

  /* start download */
  scene->addDownload();

#ifdef WIN32
  // windows has problems with Multitasking: GUI-Updates and Downloads can be done
  // only from within the main thread
//  qApp->postEvent(form, new URLRawDownloadEvent(new URLRawDownload(url.latin1(), this, NULL, allowCache)));
#else
  (new URLRawDownload(url.latin1(), this, NULL, allowCache))->run();
#endif
}

void DrawSceneObjectTexture::parseFilters(QDomNode n) {
  while(!n.isNull()) {
    /* get a texture */
    if (n.isElement() && (n.toElement().tagName() == QString("text"))) {
      textsize = n.toElement().attribute("size","10").toInt();
      float r =  n.toElement().attribute("r","1").toFloat();
      float g =  n.toElement().attribute("g","1").toFloat();
      float b =  n.toElement().attribute("b","1").toFloat();
//       float a =  n.toElement().attribute("a","0.5").toFloat();
      textcolor = QColor(int(r*255),int(g*255),int(b*255));
      text = "";

      QDomNode textNode = n.firstChild();
      while(!textNode.isNull()) {
	if (textNode.isText()) {
	  text += textNode.toText().data();
	}

	textNode = textNode.nextSibling();
      }
    }

    n = n.nextSibling();
  }
}

void DrawSceneObjectTexture::dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download) {
  printf("Download %s finished size=%i\n", url.latin1(), size);

  // Memory leak?
  QPixmap pixmaptexture;
  pixmaptexture.loadFromData((const uchar *) response, (uint) size);

// #ifdef WIN32
//   // windows has problems with fonts in alpha images
//   QImage alphaimage;
//   alphaimage.loadFromData(*image);
//   alphaimage.setAlphaBuffer(false);
//   alphaimage.loadFromData(*image);
//   pixmaptexture = alphaimage;
// #else  
//   pixmaptexture.loadFromData(*image);
// #endif
//   texture.loadFromData(QByteArray().setRawData(response, size));

  printf("alpha: %i\n", pixmaptexture.hasAlpha());

  if (text!=QString("")) {
    /* add text */
    QPainter painter(&pixmaptexture);
    QFont font;
    //   QColor red(qRgba(255,0,0,100));
    font.setPixelSize(textsize);
    painter.setPen(textcolor);
    //   painter.setPen(red);
    //   painter.setBrush(red);
    painter.setFont(font);
    //   painter.setBrush(Qt::SolidPattern);
    
    //   painter.setBackgroundMode(Qt::OpaqueMode);
    
    QStringList textlist = QStringList::split("\n", text, TRUE);
    QStringList::iterator i;
    int texty = textsize;
    for(i=textlist.begin(); i!=textlist.end(); i++) {
      painter.drawText(0,texty,*i);
      texty += textsize;
    }
  }

  texture = pixmaptexture.convertToImage();

  /* scale to a size opengl understands */
  int w = ::pow(2, int(::log((double) texture.width())/::log(2.)));
  int h = ::pow(2, int(::log((double) texture.height())/::log(2.)));
  texture = texture.smoothScale(QSize(w,h));
  texture.detach();
//   newtexture.detach();
//   texture = newtexture;
//   delete(texture);
//   delete(newtexture);

  /* tell finished download to scene */
  scene->removeDownload();
}

GLuint DrawSceneObjectTexture::getTextureID(bool forgetCreation) {
  if (!textureBound) {
    /* generate OpenGL texture context */
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);   // 2d texture (x and y size)
    /* if width or height is not 2^x, we need to rescale the image */
    
    QImage tex = QGLWidget::convertToGLFormat(texture);

  printf("scaled to %i, %i\n", tex.width(), tex.height());

#ifdef USE_GL_TEXTURE_COMPRESSION
    glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA_ARB, tex.width(), tex.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tex.bits());
#else
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tex.width(), tex.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tex.bits());
#endif
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);	// Linear Filtering
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);	// Linear Filtering
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // prevent wrap artifacts
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // prevent wrap artifacts

    if (!forgetCreation) textureBound = true;
  }

  return(textureID);
}

bool DrawSceneObjectTexture::hasAlpha() {
  return(texture.hasAlphaBuffer());
}

