#ifndef DRAWSCENEOBJECT_H
#define DRAWSCENEOBJECT_H
#include "drawScene.h"
#include <vector>

class DrawSceneObject {
 protected:
  DrawScene *scene;

  /* automatically register the object with its name */
  bool autoregister;

  std::vector<DrawSceneObject *> children;

  /** Every object can have children. This function adds them. */
  void addChild(DrawSceneObject *child);

  void erasePBRList(pbr_list *pbrlist);

 public:
  DrawSceneObject(DrawScene *scene);
  virtual ~DrawSceneObject();
  virtual void deleteChildren();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  virtual void registerXMLNames(QDomNode n);

  /** Compiles this object and all of it children into the vector */
  virtual void compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);

  void mergeArray(std::vector<openglarray *> &list1, openglarray *list2array);
  void mergeLists(std::vector<openglarray *> &list1, std::vector<openglarray *> &list2);
  void mergePBR(pbr_list *source, pbr_list *dest);

  /** Delete all objects in the list since they are clones of existing objects */
  static void deleteList(std::vector<openglarray *> &list);
};

#endif
