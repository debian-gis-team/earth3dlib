#ifndef DRAWSCENEOBJECTQUAD_H
#define DRAWSCENEOBJECTQUAD_H

#include "drawSceneObject.h"
#include "drawSceneObjectTexture.h"
#include <qdom.h>
#include "point3d.h"

class DrawSceneObjectQuad : public DrawSceneObject {
  Point3D vertexarray[4];
  Point2D texcoordarray[4];
  QString texturename;

  openglarray triangleopenglarray[2];

 public:
  DrawSceneObjectQuad(DrawScene *scene);
  virtual ~DrawSceneObjectQuad();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Compiles this object and all of it children into the vector */
  virtual void compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);
};

#endif
