#include "drawSceneObjectGroup.h"

DrawSceneObjectGroup::DrawSceneObjectGroup(DrawScene *scene)
  : DrawSceneObject(scene) {
  this->generate_pbr = false;
  pbrlist = NULL;
}

DrawSceneObjectGroup::~DrawSceneObjectGroup() {
}

void DrawSceneObjectGroup::readXMLData(QDomNode n) {
  registerXMLNames(n);

  /* n is the texture node */
  groupName = n.toElement().attribute("name");

  if (n.toElement().hasAttribute("pbr")) {
    generate_pbr = (n.toElement().attribute("pbr")=="on");
  }

  /* parse children */
  DrawSceneObject::readXMLData(n);

  /* register for pbr */
  if (generate_pbr) scene->registerGroupObjectForPBR(this);
}

void DrawSceneObjectGroup::compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* empty, the group object itself is not displayed */
}

void DrawSceneObjectGroup::compileGroup(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* draw this groups children */
  DrawSceneObject::compile(targetlist, pbrlist);

  /* erase pbr data of children */
  erasePBRList(pbrlist);

  /* add pbr data if available */
  if (this->pbrlist && pbrlist) {
    mergePBR(this->pbrlist, pbrlist);
  }
}

void DrawSceneObjectGroup::setPBR(pbr_list *pbrlist) {
  this->pbrlist = pbrlist;
}
