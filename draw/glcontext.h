#ifndef GLCONTEXT_H
#define GLCONTEXT_H
#include <qgl.h>

class GLContext : public QGLContext {
 protected:
#ifdef WIN32
  int choosePixelFormat ( void * dummyPfd, HDC pdc );
#endif

 public:
  GLContext(const QGLFormat & format, QPaintDevice * device );
  virtual ~GLContext();
};

#endif
