#ifndef DRAWSCENEOBJECTTRANSLATE_H
#define DRAWSCENEOBJECTTRANSLATE_H

#include "drawSceneObjectTransform.h"
#include "point3d.h"

class DrawSceneObjectTranslate : public DrawSceneObjectTransform {
  Point3D translation;

 public:
  DrawSceneObjectTranslate(DrawScene *scene);
  virtual ~DrawSceneObjectTranslate();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Modify single point */
  virtual Point3D transformPoint3D(Point3D &p);
};

#endif
