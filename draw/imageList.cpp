#include "imageList.h"
#include <qgl.h>
#include "urlRawDownload.h"
#include "urlRawDownloadEvent.h"
#include "globalsettings.h"

ImageList::ImageList() {
  QImage buf;
  if (!buf.load("images/webpres.png")) {
    printf("cannot find images/webpres.png\n");
  }
  
  imageTable["worldicon"] = QGLWidget::convertToGLFormat( buf );
}

QImage *ImageList::getImage(QString &iconname) {
  if (iconname == QString::null) return(NULL);//return(&imageTable["worldicon"]);

  QImage *icon = NULL;
  if (imageTable.contains(iconname)) {
    icon = &(imageTable[iconname]);
  }

  if (icon==NULL) {
    downloadIcon(iconname);
  }

  return(icon);
}

void ImageList::downloadIcon(QString &iconname) {
  if (!imageTable.contains(iconname)) {
    /* insert dummy entry to prevent more than one download */
    
    imageTable[iconname]=QImage();

#ifdef WIN32
    // windows has problems with Multitasking: GUI-Updates and Downloads can be done
    // only from within the main thread
//    qApp->postEvent(form, new URLRawDownloadEvent(new URLRawDownload(iconname.latin1(), this, NULL)));
#else
    (new URLRawDownload(iconname.latin1(), this, NULL))->run();
#endif
  }
}

void ImageList::dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download) {
  printf("imagelist received %s\n", sender);

  QImage texture;
  texture.loadFromData((unsigned char *) response, (unsigned int) size);
  texture.detach();
  imageTable[QString(sender)] = QGLWidget::convertToGLFormat( texture );
//   imageTable[QString(sender)].smoothScale(QSize(32,32));

  // redraw
  needRedraw = true;
}

